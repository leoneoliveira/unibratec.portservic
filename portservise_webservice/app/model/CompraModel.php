<?
require_once("DB.php");
class CompraModel{
	
	public function __construct() {

    }

	public function listCompras(){
		$sql = "select * from compra";
		$consulta = DB::prepare($sql);
		$consulta->execute();
		return $consulta->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function listComprasById($request){
		$sql = "select * from compra where id = :id";
		$consulta = DB::prepare($sql);
		$consulta->bindParam(":id",$request['id']);
		$consulta->execute();
		return $consulta->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function insertCompras($request){
		$data = array();
		try{
			$sql = "insert into compra (usuario_id,servico_id) 
			values (:usuario_id,:servico_id);";
			$insert = DB::prepare($sql);
			$insert->bindParam(":usuario_id",$request['usuario_id']);
			$insert->bindParam(":servico_id",$request['servico_id']);
			$insert->execute();
			$data["success"] = true;
		}catch(Exception $e){
			$data["success"] = false;
			$data["error"] = $e->getMessage();
		}
		return $data;
	}
	
	
	public function updateCompras($request){
		$data = array();
		try{
			$sql = "update compra set usuario_id = :usuario_id ,servico_id = :servico_id where id = :id;";
			$insert = DB::prepare($sql);
			$insert->bindParam(":usuario_id",$request['usuario_id']);
			$insert->bindParam(":servico_id",$request['servico_id']);
			$insert->bindParam(":id",$request['id']);
			$insert->execute();
			$data["success"] = true;
		}catch(Exception $e){
			$data["success"] = false;
			$data["error"] = $e->getMessage();
		}
		return $data;		
	}
	
	public function delteComprasById($request){
		$data = array();
		try{
			$sql = "delete from compra where id = :id";
			$consulta = DB::prepare($sql);
			$consulta->bindParam(":id",$request['id']);
			$consulta->execute();
			$data["success"] = true;
		}catch(Exception $e){
			$data["success"] = false;
			$data["error"] = $e->getMessage();
		}
		return $data;
	}
}	