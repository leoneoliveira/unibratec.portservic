<?
require_once("DB.php");
class ComentarioModel{
	
	public function __construct() {

    }

	public function listComentarios(){
		$sql = "select * from comentario";
		$consulta = DB::prepare($sql);
		$consulta->execute();
		return $consulta->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function listComentariosById($request){
		$sql = "select * from comentario where id = :id";
		$consulta = DB::prepare($sql);
		$consulta->bindParam(":id",$request['id']);
		$consulta->execute();
		return $consulta->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function insertComentarios($request){
		$data = array();
		try{
			$sql = "insert into comentario (nota,pergunta,resposta,status,servico_id,data) 
			values (:nota,:pergunta,:resposta,:status,:servico_id,:data);";
			$insert = DB::prepare($sql);
			$insert->bindParam(":nota",$request['nota']);
			$insert->bindParam(":pergunta",$request['pergunta']);
			$insert->bindParam(":resposta",$request['resposta']);
			$insert->bindParam(":status",$request['status']);
			$insert->bindParam(":servico_id",$request['servico_id']);
			$insert->bindParam(":data",$request['data']);
			$insert->execute();
			$data["success"] = true;
		}catch(Exception $e){
			$data["success"] = false;
			$data["error"] = $e->getMessage();
		}
		return $data;
	}
	
	
	public function updateComentarios($request){
		$data = array();
		try{
			$sql = "update comentario set nota = :nota,pergunta = :pergunta,resposta = :resposta,
			status = :status,servico_id = :servico_id,data = :data where id = :id;";
			$insert = DB::prepare($sql);
			$insert->bindParam(":nota",$request['nota']);
			$insert->bindParam(":pergunta",$request['pergunta']);
			$insert->bindParam(":resposta",$request['resposta']);
			$insert->bindParam(":status",$request['status']);
			$insert->bindParam(":servico_id",$request['servico_id']);
			$insert->bindParam(":data",$request['data']);
			$insert->bindParam(":id",$request['id']);
			$insert->execute();
			$data["success"] = true;
		}catch(Exception $e){
			$data["success"] = false;
			$data["error"] = $e->getMessage();
		}
		return $data;		
	}
	
	public function delteComentariosById($request){
		$data = array();
		try{
			$sql = "delete from comentario where id = :id";
			$consulta = DB::prepare($sql);
			$consulta->bindParam(":id",$request['id']);
			$consulta->execute();
			$data["success"] = true;
		}catch(Exception $e){
			$data["success"] = false;
			$data["error"] = $e->getMessage();
		}
		return $data;
	}
}