<?
require_once("DB.php");
class PerguntasRespostas{ 

    public function __construct() {
    }

	// public function listSuporte($request){
	// 	$sql = "select * from suporte";
	// 	$consulta = DB::prepare($sql);
	// 	$consulta->execute();
	// 	return $consulta->fetchAll(PDO::FETCH_ASSOC);
	// }
	
	public function manterPergunta($request){
		$data = array();
		try{
			$sql = "INSERT INTO perguntas_respostas ( pergunta
								, id_usuario
								, id_prestador
								, id_servico
								, data_hora
								, status
								) VALUES (
								 :pergunta
								,:id_usuario
								,:id_prestador
								,:id_servico
								,NOW()
								,'A'
								);";
			$insert = DB::prepare($sql);

			$insert->bindParam(":pergunta",$request['pergunta']);
			$insert->bindParam(":id_usuario",$request['id_usuario']);
			$insert->bindParam(":id_prestador",$request['id_prestador']);
			$insert->bindParam(":id_servico",$request['id_servico']);


			$insert->execute();

 		}
	    catch(Exception $e){
			$data["success"] = false;
			$data["error"] = $e->getMessage();
		}
		return $data;;
	}
	
}