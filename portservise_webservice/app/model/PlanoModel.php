<?
require_once("DB.php");
class PlanoModel {
	
	public function __construct() {

    }

    public function manterPlano($request){
    	$data = array();
    	try{
	    	if(isset($request['codplano'])){
	    		$sql = "UPDATE plano SET nome = :nome, descricao = :descricao, preco = :preco, 
	    				status = :status, preco = :preco WHERE plano_id = :id ";
	    		$data["tipo"] = "update";
	    	}else{
	    		$sql = "INSERT INTO plano (nome, descricao, preco, status ) 
	    				VALUES (:nome, :descricao, :preco, :status) ";
	    		$data["tipo"] = "inserir";
	    	}

	    	$insert = DB::prepare($sql);
	    	$insert->bindParam(":nome", $request['nome']);
	    	$insert->bindParam(":descricao",$request['descricao']);
	    	$insert->bindParam(":preco",$request['preco']);
	    	$insert->bindParam(":status", $request['status']);

	    	if(isset($request['codplano'])){
	    		$insert->bindParam(":id", $request['codplano']);
	    	}

	    	$insert->execute();
	    }
	    catch(Exception $e){
			$data["success"] = false;
			$data["error"] = $e->getMessage();
		}
		return $data;
    }

    public function aderirPlano($request){
    	$data = array();
    	try {
    		$sql = "INSERT INTO planopagamento (plano_id, formapagamento_id, usuario_id, valor, dias, validade, data, parcelas, 
    			bandeira, nomecartao, codigoseguranca, numerocartao, mes, ano, aprovado) VALUES (:codplano, :codformapagamento, 
    			:codusuario, :valor, :dias, :validade, :data, :parcelas, :bandeira, :nomecartao, :codigoseguranca, :numerocartao,
    			 :mes, :ano, 'P') ";
			$insert = DB::prepare($sql);
	    	$insert->bindParam(":codplano", $request['codplano']);
	    	$insert->bindParam(":codformapagamento",$request['codformapagamento']);
	    	$insert->bindParam(":codusuario",$request['codusuario']);
	    	$insert->bindParam(":valor", $request['valor']);
	    	$insert->bindParam(":data", $request['data']);
	    	$insert->bindParam(":dias", $request['dias']);

	    	$validade = date("Ymd", strtotime($request['data'].'+ '.$request['dias'].' days'));
	    	$insert->bindParam(":validade", $validade);

	    	isset($request['parcelas']) ? $parcelas = $request['parcelas'] : $parcelas =  "";
	    	isset($request['bandeira']) ? $bandeira = $request['bandeira'] : $bandeira =  "";
	    	isset($request['nomecartao']) ? $nomecartao = $request['nomecartao'] : $nomecartao =  "";
	    	isset($request['codigoseguranca']) ? $codigoseguranca = $request['codigoseguranca'] : $codigoseguranca =  "";
	    	isset($request['numerocartao']) ? $numerocartao = $request['numerocartao'] : $numerocartao =  "";
	    	isset($request['mes']) ? $mes = $request['mes'] : $mes =  "";
	    	isset($request['ano']) ? $ano = $request['ano'] : $ano =  "";

	    	$insert->bindParam(":parcelas", $parcelas);
	    	$insert->bindParam(":bandeira", $bandeira);
	    	$insert->bindParam(":nomecartao", $nomecartao);
	    	$insert->bindParam(":codigoseguranca", $codigoseguranca);
	    	$insert->bindParam(":numerocartao", $numerocartao);
	    	$insert->bindParam(":mes", $mes);
	    	$insert->bindParam(":ano", $ano);

	    	$insert->execute();

	    	$data['sucess'] = true;
    	} catch (Exception $e) {
    		$data["success"] = false;
			$data["error"] = $e->getMessage();
    	}
    	return $data;
    }
	
	public function listPlanos($request){

		$sql = "SELECT * FROM plano WHERE status IN ('A','I') ";
		
		if(isset($request['nome'])){
			$sql .= " AND c.categoria_descricao LIKE CONCAT('%', :nome, '%') ";
		}
		if(isset($request['filtros'])){
			$sql .= $request['filtros'];
		}

		$consulta = DB::prepare($sql);
		if(isset($request['nome'])){
			$consulta->bindParam(":nome",$request['nome']);
		}	
		
		$consulta->execute();
		return $consulta->fetchAll(PDO::FETCH_ASSOC);
	}

	public function listPendentes($request){

		$sql = "SELECT planopagamento.*, usuario.nome AS nomeusuario, formapagamento.nome AS nomepagamento, plano.nome AS nomeplano, plano.qtdanuncio 
				FROM planopagamento, usuario, formapagamento, plano
				WHERE planopagamento.usuario_id = usuario.id AND planopagamento.plano_id = plano.plano_id 
				AND planopagamento.formapagamento_id = formapagamento.formapagamento_id AND planopagamento.aprovado = 'P' ";
		
		if(isset($request['codplano'])){
			$sql .= " AND planopagamento.plano_id = :codplano ";
		}
		if(isset($request['codusuario'])){
			$sql .= " AND planopagamento.usuario_id = :codusuario ";
		}
		if(isset($request['codformapagamento'])){
			$sql .= " AND planopagamento.formapagamento_id = :codformapagamento ";
		}

		if(isset($request['filtros'])){
			$sql .= $request['filtros'];
		}

		$consulta = DB::prepare($sql);

		if(isset($request['codplano'])){
			$consulta->bindParam(":codplano",$request['codplano']);
		}
		if(isset($request['codusuario'])){
			$consulta->bindParam(":codusuario",$request['codusuario']);
		}
		if(isset($request['codformapagamento'])){
			$consulta->bindParam(":codformapagamento",$request['codformapagamento']);
		}
		
		$consulta->execute();
		return $consulta->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function incrementaPlano($request){
		$sql = "UPDATE plano SET adqueridos = adqueridos + 1 WHERE plano_id = :id";
		$insert = DB::prepare($sql);
		$insert->bindParam(":id",$request['id']);
		$insert->execute();
	}

	public function listPlanosById($request){
		$data = array();
		try{
			$sql = "SELECT * FROM plano WHERE plano_id = " . $request['codplano'];
			$consulta = DB::prepare($sql);
			$consulta->execute();
			return $consulta->fetchAll(PDO::FETCH_ASSOC);
		}catch(Exception $e){
			$data["request"] = $request;
			$data["error"] = $e->getMessage();
			$data["sql"] = $sql;
			return $data;
		}
	}

	public function consultarRows($request){
		$sql = "SELECT COUNT(*) AS qtd
				FROM plano WHERE status IN ('A','I') ";
		
		if(isset($request['nome'])){
			$sql .= " AND nome LIKE CONCAT('%', :nome, '%') ";
		}
		
		$consulta = DB::prepare($sql);
		if(isset($request['nome'])){
			$consulta->bindParam(":nome",$request['nome']);
		}	
		$consulta->execute();
		$qtd = $consulta->fetch(PDO::FETCH_ASSOC);
		return $qtd['qtd'];
	}

	public function consultarRowsPendentes($request){
		$sql = "SELECT count(*) AS qtd FROM planopagamento, usuario, formapagamento, plano
				WHERE planopagamento.usuario_id = usuario.id AND planopagamento.plano_id = plano.plano_id 
				AND planopagamento.formapagamento_id = formapagamento.formapagamento_id AND planopagamento.aprovado = 'P' ";
		
		if(isset($request['codplano'])){
			$sql .= " AND planopagamento.plano_id = :codplano ";
		}
		if(isset($request['codusuario'])){
			$sql .= " AND planopagamento.usuario_id = :codusuario ";
		}
		if(isset($request['codformapagamento'])){
			$sql .= " AND planopagamento.formapagamento_id = :codformapagamento ";
		}

		if(isset($request['filtros'])){
			$sql .= $request['filtros'];
		}

		$consulta = DB::prepare($sql);

		if(isset($request['codplano'])){
			$consulta->bindParam(":codplano",$request['codplano']);
		}
		if(isset($request['codusuario'])){
			$consulta->bindParam(":codusuario",$request['codusuario']);
		}
		if(isset($request['codformapagamento'])){
			$consulta->bindParam(":codformapagamento",$request['codformapagamento']);
		}

		$consulta->execute();
		$qtd = $consulta->fetch(PDO::FETCH_ASSOC);
		return $qtd['qtd'];
	}

	public function aprovarReprovar($request){
		$data = array();
		try {
			$sql = "UPDATE planopagamento SET aprovado = :aprovado WHERE planopagamento_id = :codplanopagamento ";
			$insert = DB::prepare($sql);

			$insert->bindParam(":aprovado", $request['aprovado']);
			$insert->bindParam(":codplanopagamento", $request['codplanoplagamento']);

			$insert->execute();

			$insert = "";
			$sql = "";
			if($request['aprovado'] == "A"){
				$sql = "UPDATE usuario SET plano_id = :codplano WHERE id = :codusuario ";
				$insert = DB::prepare($sql);

				$insert->bindParam(":codplano", $request['codplano']);
				$insert->bindParam(":codusuario", $request['codusuario']);

				$insert->execute();

				$msg= "Pagamento Aprovado com sucesso!";
			}else{
				$msg= "Pagamento Reprovado com sucesso!";
			}
			$data["sucess"] = true;
			$data["msg"] = $msg;
		} catch (Exception $e) {
			$data["success"] = false;
			$data["error"] = $e->getMessage();
		}

		return $data;
	}
}