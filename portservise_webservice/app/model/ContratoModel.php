<?
require_once("DB.php");
class ContratoModel {
	
	public function __construct() {

    }
	
	public function sendContrato($request){
		$data = array();
		try{
			$contratado = $this->servicoExistente($request);
			if($contratado == 0){
				$sql = "insert into envio_contrato (cliente_id,prestador_id,servico_id) 
				values (:cliente_id,:prestador_id,:servico_id);";
				$insert = DB::prepare($sql);
				$insert->bindParam(":cliente_id",$request['cliente_id']);
				$insert->bindParam(":prestador_id",$request['prestador_id']);
				$insert->bindParam(":servico_id",$request['servico_id']);
				$insert->execute();				
				$data["success"] = true;
				$dadosComplementares = $this->nomeContratado($request['prestador_id']);
				$data["nome"] = $dadosComplementares[0]["nome"];
				$data["celular"] = $dadosComplementares[0]["celular"];
				$data["telefone"] = $dadosComplementares[0]["telefone"];
				$data["email"] = $dadosComplementares[0]["email"];
			}else{
				$data["success"] = false;
				$data["error"] = "Esse é seu propio Anuncio!!";	
				$data["debug"] = $contratado;
			}
		}catch(Exception $e){
			$data["success"] = false;
			$data["error"] = $e->getMessage();
		}
		return $data;		
	}
	
	public function servicoExistente($request){
		$sql = "SELECT count(*) qtd from servico where usuario_id = :usuario_id ";
		$sql .= " and id = :id ";
		$consulta = DB::prepare($sql);
		$consulta->bindParam(":usuario_id",$request['cliente_id']);
		$consulta->bindParam(":id",$request['servico_id']);
		$consulta->execute();
		$qtd = $consulta->fetchAll(PDO::FETCH_ASSOC);
		return $qtd[0]["qtd"]; 
	}
	
	public function nomeContratado($id){
		$sql = "SELECT nome,celular,telefone,email FROM `usuario` WHERE id = :id;";
		$consulta = DB::prepare($sql);
		$consulta->bindParam(":id",$id);
		$consulta->execute();
		return $consulta->fetchAll(PDO::FETCH_ASSOC);
		//$nome = $consulta->fetchAll(PDO::FETCH_ASSOC);
		//return	$nome[0]['nome'];
	}
} 