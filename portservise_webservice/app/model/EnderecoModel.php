<?
require_once("DB.php");
class EnderecoModel{
	
	public function __construct() {

    }

	public function listEnderecos(){
		$sql = "select * from endereco";
		$consulta = DB::prepare($sql);
		$consulta->execute();
		return $consulta->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function listEnderecosById($request){
		$sql = "select * from endereco where id = :id";
		$consulta = DB::prepare($sql);
		$consulta->bindParam(":id",$request['id']);
		$consulta->execute();
		return $consulta->fetchAll(PDO::FETCH_ASSOC);
	}

	public function listEnderecosUsuario(){
		$sql = "select * from endereco e , usuario u where u.id = e.usuario_id ";
		$consulta = DB::prepare($sql);
		$consulta->execute();
		return $consulta->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function listEnderecosUsuarioById($request){
		$sql = "select * from endereco e , usuario u where u.id = e.usuario_id and e.id = :id";
		$consulta = DB::prepare($sql);
		$consulta->bindParam(":id",$request['id']);
		$consulta->execute();
		return $consulta->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function listEnderecosUsuarioById2($request){
		$sql = "select * from endereco e , usuario u where u.id = e.usuario_id and u.id = :id";
		$consulta = DB::prepare($sql);
		$consulta->bindParam(":id",$request['id']);
		$consulta->execute();
		return $consulta->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function countEnderecoUsuarioById($request){
		$sql = "select count(*) as qtd from endereco e where usuario_id = :usuario_id";
		$consulta = DB::prepare($sql);
		$consulta->bindParam(":usuario_id",$request['usuario_id']);
		$consulta->execute();
		return $consulta->fetch(PDO::FETCH_ASSOC);
	}
	
	public function enderecoUsuarioById($request){
		$qtd = $this->countEnderecoUsuarioById($request);
		$data = array();
		$data["success"] = false;
		if($qtd['qtd'] == 0){
			$data = $this->insertEnderecos($request);
		}else{
			$data = $this->updateEndereco($request);
		}
		return $data;
	}
	
	public function insertEnderecos($request){
		$data = array();
		try{
			$sql = "insert into endereco (endereco,numero,bairro,
			usuario_id,cep,cidade,uf) 
			values (:endereo,:numero,:bairro,:usuario_id,:cep,:cidade,:uf);";
			$insert = DB::prepare($sql);
			$insert->bindParam(":endereo",$request['endereco']);
			$insert->bindParam(":numero",$request['numero']);
			$insert->bindParam(":bairro",$request['bairro']);
			$insert->bindParam(":usuario_id",$request['usuario_id']);
			$insert->bindParam(":cep",$request['cep']);
			$insert->bindParam(":cidade",$request['cidade']);
			$insert->bindParam(":uf",$request['uf']);
			$insert->execute();
			$data["success"] = true;
			$request["id"] = DB::lastInsertId();
			$data["Adress"] = $request;
		}catch(Exception $e){
			$data["status"] = "insert";
			$data["success"] = false;
			$data["error"] = $e->getMessage();
		}
		return $data;
	}
	
	
	public function updateEnderecos($request){
		$data = array();
		try{
			$sql = "update endereco set endereco = :endereco,numero = :numero ,bairro = :bairro,
			usuario_id = :usuario_id, cep = :cep, cidade = :cidade, uf = :uf where id = :id;";
			$insert = DB::prepare($sql);
			$insert->bindParam(":endereco",$request['endereco']);
			$insert->bindParam(":numero",$request['numero']);
			$insert->bindParam(":bairro",$request['bairro']);
			$insert->bindParam(":usuario_id",$request['usuario_id']);
			$insert->bindParam(":cep",$request['cep']);
			$insert->bindParam(":cidade",$request['cidade']);
			$insert->bindParam(":uf",$request['uf']);
			$insert->bindParam(":id",$request['id']);
			$insert->execute();
			$data["success"] = true;
			$data["Adress"] = $request;
		}catch(Exception $e){
			$data["status"] = "update";
			$data["success"] = false;
			$data["error"] = $e->getMessage();
		}
		return $data;		
	}
	
	public function updateEndereco($request){
		$data = array();
		try{
			$sql = "update endereco set endereco = :endereco,numero = :numero ,bairro = :bairro,
			cep = :cep, cidade = :cidade, uf = :uf where usuario_id = :usuario_id";
			$insert = DB::prepare($sql);
			$insert->bindParam(":endereco",$request['endereco']);
			$insert->bindParam(":numero",$request['numero']);
			$insert->bindParam(":bairro",$request['bairro']);
			$insert->bindParam(":usuario_id",$request['usuario_id']);
			$insert->bindParam(":cep",$request['cep']);
			$insert->bindParam(":cidade",$request['cidade']);
			$insert->bindParam(":uf",$request['uf']);
			$insert->execute();
			$data["success"] = true;
			$data["Adress"] = $request;
		}catch(Exception $e){
			$data["status"] = "update";
			$data["success"] = false;
			$data["error"] = $e->getMessage();
		}
		return $data;		
	}
	
	public function delteEnderecosById($request){
		$data = array();
		try{
			$sql = "delete from endereco where id = :id";
			$consulta = DB::prepare($sql);
			$consulta->bindParam(":id",$request['id']);
			$consulta->execute();
			$data["success"] = true;
		}catch(Exception $e){
			$data["success"] = false;
			$data["error"] = $e->getMessage();
		}
		return $data;
	}	
}