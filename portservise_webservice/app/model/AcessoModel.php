<?
require_once("DB.php");
class AcessoModel{
	
	public function __construct() {
    }
	
	public function verificarAcessoUsuarioHoje($request){
		$sql = "select count(*) as qtd from acessos 
		where id_usuario = :id_usuario and
		data = :data";
		$data = date("Y-m-d");
		$consulta = DB::prepare($sql);
		$consulta->bindParam(":id_usuario",$request['id_usuario']);
		$consulta->bindParam(":data",$data);
		$consulta->execute();
		$acesso = $consulta->fetch(PDO::FETCH_ASSOC);
		if($acesso['qtd'] == 0){
			$this->cadastrarAcessoUsuarioHoje($request);
		}
	}
	
	public function cadastrarAcessoUsuarioHoje($request){
		$sql = "insert into acessos (id_usuario,acao,conteudo,data) 
		values (:id_usuario,:acao,:conteudo,now());";
		$data = date("Y-m-d");
		$consulta = DB::prepare($sql);
		$consulta->bindParam(":id_usuario",$request['id_usuario']);
		$consulta->bindParam(":acao",$request['acao']);
		$consulta->bindParam(":conteudo",$request['conteudo']);
		//$consulta->bindParam(":data",$data);
		$consulta->execute();
	}
	
	public function cadastrarAcessoHoje($request){
		$sql = "insert into acessos (acao,conteudo,data) 
		values (:acao,:conteudo,now());";
		$data = date("Y-m-d");
		$consulta = DB::prepare($sql);
		$consulta->bindParam(":acao",$request['acao']);
		$consulta->bindParam(":conteudo",$request['conteudo']);
		//$consulta->bindParam(":data",$data);
		$consulta->execute();		
	}
}