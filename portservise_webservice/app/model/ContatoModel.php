<?
require_once("DB.php");
class ContatoModel{
	public function __construct() {

    }

	public function listContatos(){
		$sql = "select * from contato";
		$consulta = DB::prepare($sql);
		$consulta->execute();
		return $consulta->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function listContatosById($request){
		$sql = "select * from contato where id = :id";
		$consulta = DB::prepare($sql);
		$consulta->bindParam(":id",$request['id']);
		$consulta->execute();
		return $consulta->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function insertContatos($request){
		$data = array();
		try{
			$sql = "insert into contato (telefone,operadora,status,usuario_id) 
			values (:telefone,:operadora,:status,:usuario_id);";
			$insert = DB::prepare($sql);
			$insert->bindParam(":telefone",$request['telefone']);
			$insert->bindParam(":operadora",$request['operadora']);
			$insert->bindParam(":status",$request['status']);
			$insert->bindParam(":usuario_id",$request['usuario_id']);
			$insert->execute();
			$data["success"] = true;
		}catch(Exception $e){
			$data["success"] = false;
			$data["error"] = $e->getMessage();
		}
		return $data;
	}
	
	
	public function updateContatos($request){
		$data = array();
		try{
			$sql = "update contato set telefone = :telefone,operadora = :operadora,
			status = :status ,usuario_id = :usuario_id where id = :id;";
			$insert = DB::prepare($sql);
			$insert->bindParam(":telefone",$request['telefone']);
			$insert->bindParam(":operadora",$request['operadora']);
			$insert->bindParam(":status",$request['status']);
			$insert->bindParam(":usuario_id",$request['usuario_id']);
			$insert->bindParam(":id",$request['id']);
			$insert->execute();
			$data["success"] = true;
		}catch(Exception $e){
			$data["success"] = false;
			$data["error"] = $e->getMessage();
		}
		return $data;		
	}
	
	public function delteContatosById($request){
		$data = array();
		try{
			$sql = "delete from contato where id = :id";
			$consulta = DB::prepare($sql);
			$consulta->bindParam(":id",$request['id']);
			$consulta->execute();
			$data["success"] = true;
		}catch(Exception $e){
			$data["success"] = false;
			$data["error"] = $e->getMessage();
		}
		return $data;
			
}