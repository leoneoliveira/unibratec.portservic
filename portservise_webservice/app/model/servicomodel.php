<?
require_once("DB.php");
class ServicoModel {

    public function __construct() {

    }
	
	public function listServicos(){
		$sql = "select * from servico";
		$consulta = DB::prepare($sql);
		$consulta->execute();
		return $consulta->fetchAll(PDO::FETCH_ASSOC);
	}

	// BACKEND
		public function listServicosPendentes($request){
			$sql = "SELECT servico.*, usuario.nome AS nomeusuario 
					FROM servico, usuario 
					WHERE servico.usuario_id = usuario.id AND servico.status = 'P' ";
			
			if(isset($request['filtros'])){
				$sql .= $request['filtros'];
			}
			$consulta = DB::prepare($sql);

			$consulta->execute();
			return $consulta->fetchAll(PDO::FETCH_ASSOC);
		}
		public function consultarRows($request){
			$sql = "SELECT COUNT(*) AS qtd 
					FROM servico, usuario 
					WHERE servico.usuario_id = usuario.id AND servico.status = 'P' ";	
			$consulta = DB::prepare($sql);

			$consulta->execute();
			$qtd = $consulta->fetch(PDO::FETCH_ASSOC);

			return $qtd['qtd'];
		}
	//
	public function listServicosById($request){
		$sql = "select * from servico where id = :id";
		$consulta = DB::prepare($sql);
		$consulta->bindParam(":id",$request['id']);
		$consulta->execute();
		return $consulta->fetchAll(PDO::FETCH_ASSOC);
	}

	public function aprovarServico($request){
		$data = array();
		try {
			$sql = "UPDATE servico SET status = 'A' WHERE id = :id ";
			$insert = DB::prepare($sql);
			$insert->bindParam(":id", $request['codservico']);
			$insert->execute();

			$data["success"] = true;
		}catch(Exception $e){
			$data["success"] = false;
			$data["error"] = $e->getMessage();
		}
		return $data;
	}

	public function listServicosByUsuarioId($request){
		$sql = "select * from servico where usuario_id = :usuario_id";
		$consulta = DB::prepare($sql);
		$consulta->bindParam(":usuario_id",$request['usuario_id']);
		$consulta->execute();
		return $consulta->fetchAll(PDO::FETCH_ASSOC);
	}


	
	public function listServicosByUsuarioId2($request){
		$sql = "SELECT c.categoria_descricao,c.categoria_self_join , s.id AS id_servico, s.status situacao, s. *, contadorVisulAninco(s.usuario_id,s.id) as visualizacao 
		FROM servico s
		INNER JOIN categoria c ON c.categoria_id = s.categoria_id
		INNER JOIN usuario u ON u.id = s.usuario_id
		WHERE u.id = :id";
		$consulta = DB::prepare($sql);
		$consulta->bindParam(":id",$request['id']);
		$consulta->execute();
		//var_dump($request);
		$listServicosByUsuario = array();
		while ($obj = $consulta->fetch(PDO::FETCH_ASSOC)){
			$obj["fotos"] = $this->listServicosImagem2($obj["id_servico"]);
			array_push($listServicosByUsuario,$obj);
		}
		//return $consulta->fetchAll(PDO::FETCH_ASSOC);
		return $listServicosByUsuario;
	}
	
	public function listServicosImagem2($id){
		$sql = "SELECT * FROM servico_imagem si where si.servico_id = :id";
		$consulta = DB::prepare($sql);
		$consulta->bindParam(":id",$id);
		$consulta->execute();
		return $consulta->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function listServicosImagem($request){
		$data = array();
		try{
			$sql = "SELECT c.categoria_descricao, c.categoria_self_join , s.id AS id_servico, s. * 
			FROM servico s
			INNER JOIN categoria c ON c.categoria_id = s.categoria_id
			INNER JOIN usuario u ON u.id = s.usuario_id
			WHERE s.id <= :id
			";
			if($request["cidade"] != ""){
				$sql .= "and s.cidade = :cidade ";
			}
			if($request["bairro"] != ""){
				$sql .= "and s.bairro = :bairro ";
			}
			if($request["cep"] != ""){
				$sql .= "and s.cep = :cep ";
			}
			if($request["uf"] != ""){
				$sql .= "and s.uf = :uf ";
			}
			if($request["valor_inicial"] != ""){
				$sql .= "and s.valor >= :valor_inicial ";
			}
			if($request["valor_final"] != ""){
				$sql .= "and s.valor <= :valor_final ";
			}
			if($request["palavra_chave"] != ""){
				$sql .= "and s.palavra_chave = :palavra_chave ";
			}	
			if($request["experiencia"] != ""){
				$sql .= "and s.experiencia = :experiencia ";
			}	
			if($request["categoria_id"] != ""){
				$count = $this->listSubCategorias($request["categoria_id"]);
				if(count($count) > 0){
					foreach($count as $linha){
						$request["categoria_id"] .= $linha["categoria_id"].",";
					}
					$request["categoria_id"] = substr($request["categoria_id"], 0, -1);
				}
				$sql .= "and s.categoria_id in(".$request["categoria_id"].") ";
			}			
			
			$sql .= " and s.status = 'A' ";
			
			$sql .= "ORDER BY s.id DESC LIMIT 10";
			$consulta = DB::prepare($sql);
			$consulta->bindParam(":id",$request['id']);
			if($request["cidade"] != ""){
				$consulta->bindParam(":cidade",$request['cidade']);
			}
			if($request["bairro"] != ""){
				$consulta->bindParam(":bairro",$request['bairro']);
			}
			if($request["cep"] != ""){
				$consulta->bindParam(":cep",$request['cep']);
			}
			if($request["uf"] != ""){
				$consulta->bindParam(":uf",$request['uf']);
			}			
			if($request["valor_inicial"] != ""){
				$consulta->bindParam(":valor_inicial",$request['valor_inicial']);
			}
			if($request["valor_final"] != ""){
				$consulta->bindParam(":valor_final",$request['valor_final']);
			}		
			if($request["palavra_chave"] != ""){
				$consulta->bindParam(":palavra_chave",$request['palavra_chave']);
			}	
			if($request["experiencia"] != ""){
				$consulta->bindParam(":experiencia",$request['experiencia']);
			}			
			$consulta->execute();
			//var_dump($request);
			$listServicosImagem = array();
			/*só para debug
			$listServicosImagem[10] = $sql;
			$listServicosImagem[11] = $request;
			/*só para debug*/
			while ($obj = $consulta->fetch(PDO::FETCH_ASSOC)){
				$obj["fotos"] = $this->listServicosImagem2($obj["id_servico"]);
				array_push($listServicosImagem,$obj);
			}
			return $listServicosImagem;
		}catch(Exception $e){
			$data["error"] = $e->getMessage();
			$data["sql"] = $sql;
			return $data;
		}
	}
	
	public function listSubCategorias($id){
		$sql = "SELECT * FROM  categoria WHERE  categoria_self_join = :id";
		$consulta = DB::prepare($sql);
		$consulta->bindParam(":id",$id);
		$consulta->execute();
		return $consulta->fetchAll(PDO::FETCH_ASSOC);		
	}

	public function listServicosImagemComfiltros($request){
		$sql = "SELECT c.categoria_descricao, s.id AS id_servico, s. * 
		FROM servico s
		INNER JOIN categoria c ON c.categoria_id = s.categoria_id
		INNER JOIN usuario u ON u.id = s.usuario_id
		";
		$sql .= "LIMIT 10";
		$consulta = DB::prepare($sql);
		$consulta->execute();
		//var_dump($request);
		$listServicosImagem = array();
		while ($obj = $consulta->fetch(PDO::FETCH_ASSOC)){
			$obj["fotos"] = $this->listServicosImagem2($obj["id_servico"]);
			array_push($listServicosImagem,$obj);
		}
		return $listServicosImagem;
	}	
	
	public function listServicosImagemById($request){
		$sql = "SELECT c.categoria_descricao, s.id AS id_servico, s. * , si. * 
		FROM servico s
		INNER JOIN categoria c ON c.categoria_id = s.categoria_id
		LEFT JOIN servico_imagem si ON si.servico_id = s.id
		where s.id = :id
		limit 10
		";
		$consulta = DB::prepare($sql);
		$consulta->bindParam(":id",$request['id']);
		$consulta->execute();
		return $consulta->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function quantidadeServicos(){
		$sql = "select count(*) as quantidade from servico";
		$consulta = DB::prepare($sql);
		$consulta->execute();
		return $consulta->fetchAll(PDO::FETCH_ASSOC);		
	}
	
	public function lastIdservico(){
		$sql = "SELECT id FROM servico ORDER BY id DESC LIMIT 1";
		$consulta = DB::prepare($sql);
		$consulta->execute();
		return $consulta->fetch(PDO::FETCH_ASSOC);		
	}
	
	public function firstIdservico(){
		$sql = "SELECT id FROM servico ORDER BY id ASC LIMIT 1";
		$consulta = DB::prepare($sql);
		$consulta->execute();
		return $consulta->fetch(PDO::FETCH_ASSOC);		
	}
	
	public function insertServicos($request){
		$data = array();
		try{
			$sql = "insert into servico (
			titulo,descricao,
			usuario_id,status,
			categoria_id,palavra_chave,
			cep,cidade,uf,privacidade,
			aceita_termos,endereco,valor,bairro,
			experiencia
			) 
			values (
			:titulo,:descricao,:usuario_id,
			:status,
			:categoria_id,:palavra_chave,
			:cep,:cidade,:uf,:privacidade,
			:aceita_termos,:endereco,:valor,:bairro,
			:experiencia
			);";
			$insert = DB::prepare($sql);
			$insert->bindParam(":titulo",$request['titulo']);
			$insert->bindParam(":descricao",$request['descricao']);
			$insert->bindParam(":categoria_id",$request['categoria_id']);
			$insert->bindParam(":usuario_id",$request['usuario_id']);
			$insert->bindParam(":status",$request['status']);
			$insert->bindParam(":palavra_chave",$request['palavra_chave']);
			$insert->bindParam(":cep",$request['cep']);
			$insert->bindParam(":cidade",$request['cidade']);
			$insert->bindParam(":uf",$request['uf']);
			$insert->bindParam(":privacidade",$request['privacidade']);
			$insert->bindParam(":aceita_termos",$request['aceita_termos']);
			$insert->bindParam(":endereco",$request['endereco']);
			$insert->bindParam(":valor",$request['valor']);
			$insert->bindParam(":bairro",$request['bairro']);
			$insert->bindParam(":experiencia",$request['experiencia']);
			$insert->execute();
			$data["success"] = true;
			$data["id_servico"]= DB::lastInsertId();
		}catch(Exception $e){
			$data["success"] = false;
			$data["error"] = $e->getMessage();
		}
		return $data;
	}
	
	
	public function updateServicos($request){
		$data = array();
		try{
			$sql = "update servico set titulo = :titulo,descricao = :descricao ,valor = :valor,
			usuario_id = :usuario_id, status = :status where id = :id;";
			$insert = DB::prepare($sql);
			$insert->bindParam(":titulo",$request['titulo']);
			$insert->bindParam(":descricao",$request['descricao']);
			$insert->bindParam(":valor",$request['valor']);
			$insert->bindParam(":usuario_id",$request['usuario_id']);
			$insert->bindParam(":status",$request['status']);
			$insert->bindParam(":id",$request['id']);
			$insert->execute();
			$data["success"] = true;
		}catch(Exception $e){
			$data["success"] = false;
			$data["error"] = $e->getMessage();
		}
		return $data;		
	}
	
	public function delteServicosById($request){
		$data = array();
		try{
			$sql = "delete from servico where id = :id";
			$consulta = DB::prepare($sql);
			$consulta->bindParam(":id",$request['id']);
			$consulta->execute();
			$data["success"] = true;
		}catch(Exception $e){
			$data["success"] = false;
			$data["error"] = $e->getMessage();
		}
		return $data;
	}		
}
