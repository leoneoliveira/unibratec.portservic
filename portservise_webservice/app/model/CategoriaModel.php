<?
require_once("DB.php");
class CategoriaModel {
	
	public function __construct() {

    }

    public function manterCategoria($request){
    	$data = array();
    	try{
	    	if(isset($request['codcategoria'])){
	    		$sql = "UPDATE categoria SET categoria_descricao = :nome, categoria_self_join = :codcategoriapai, 
	    				status = :status WHERE categoria_id = :id ";
	    		$data["tipo"] = "update";
	    	}else{
	    		$sql = "INSERT INTO categoria(categoria_descricao, categoria_self_join, status ) 
	    				VALUES (:nome, :codcategoriapai, :status) ";
	    		$data["tipo"] = "inserir";
	    	}

	    	$insert = DB::prepare($sql);
	    	$insert->bindParam(":nome", $request['nome']);
	    	$insert->bindParam(":codcategoriapai",$request['codcategoriapai']);
	    	$insert->bindParam(":status", $request['status']);

	    	if(isset($request['codcategoria'])){
	    		$insert->bindParam(":id", $request['codcategoria']);
	    	}

	    	$insert->execute();
	    }
	    catch(Exception $e){
			$data["success"] = false;
			$data["error"] = $e->getMessage();
		}
		return $data;
    }
	
	public function listCategorias($request){

		$sql = "SELECT c.*, p.categoria_descricao AS nomepai 
				FROM categoria c
				LEFT JOIN categoria p ON c.categoria_self_join = p.categoria_id
				WHERE c.status IN ('A','I') ";
		
		if(isset($request['codcategoriapai'])){
			$sql .= " AND c.categoria_self_join = :codcategoriapai ";
		}
		if(isset($request['nome'])){
			$sql .= " AND c.categoria_descricao LIKE CONCAT('%', :nome, '%') ";
		}
		if(isset($request['filtros'])){
			$sql .= $request['filtros'];
		}

		$consulta = DB::prepare($sql);
		if(isset($request['codcategoriapai'])){
			$consulta->bindParam(":codcategoriapai",$request['codcategoriapai']);
		}
		if(isset($request['nome'])){
			$consulta->bindParam(":nome",$request['nome']);
		}	
		
		$consulta->execute();
		return $consulta->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function listMainCategorias(){
		$sql = "SELECT * FROM categoria WHERE categoria_self_join IS NULL AND status = 'A'";
		$consulta = DB::prepare($sql);
		$consulta->execute();
		return $consulta->fetchAll(PDO::FETCH_ASSOC);		
	}

	public function comboBox(){
		$objCategorias = array();
		$listaObjCategorias = array();
		foreach($this->listMainCategorias() as $key => $obj){
			$objCategorias["categoria_id"] = $obj["categoria_id"];
			$objCategorias["categoria_descricao"] = $obj["categoria_descricao"];
			$objCategorias["categoria_subs"] = $this->listCategoriasBySelfjoin($obj["categoria_id"]);
			array_push($listaObjCategorias,$objCategorias);
		}
		return $listaObjCategorias;
	}
	
	public function incrementaCategoria($request){
		$sql = "UPDATE categoria SET visita = visita + 1 WHERE categoria_id = :id";
		$insert = DB::prepare($sql);
		$insert->bindParam(":id",$request['id']);
		$insert->execute();
	}
	
	public function listCategoriasBySelfjoin($id){
		$sql = "SELECT * FROM  categoria WHERE  categoria_self_join = ".$id;
		$consulta = DB::prepare($sql);
		$consulta->execute();
		return $consulta->fetchAll(PDO::FETCH_ASSOC);
	}

	public function listCategoriasById($request){
		$data = array();
		try{
			$sql = "SELECT * FROM categoria WHERE categoria_id = " . $request['codcategoria'];
			$consulta = DB::prepare($sql);
			$consulta->execute();
			return $consulta->fetchAll(PDO::FETCH_ASSOC);
		}catch(Exception $e){
			$data["request"] = $request;
			$data["error"] = $e->getMessage();
			$data["sql"] = $sql;
			return $data;
		}
	}

	public function consultarRows($request){
		$sql = "SELECT COUNT(*) AS qtd
				FROM categoria c
				LEFT JOIN categoria p ON c.categoria_self_join = p.categoria_id
				WHERE c.status IN ('A','I') ";
		
		if(isset($request['codcategoriapai'])){
			$sql .= " AND c.categoria_self_join = :codcategoriapai ";
		}
		if(isset($request['nome'])){
			$sql .= " AND c.categoria_descricao LIKE CONCAT('%', :nome, '%') ";
		}
		
		$consulta = DB::prepare($sql);
		if(isset($request['codcategoriapai'])){
			$consulta->bindParam(":codcategoriapai",$request['codcategoriapai']);
		}
		if(isset($request['nome'])){
			$consulta->bindParam(":nome",$request['nome']);
		}	
		$consulta->execute();
		$qtd = $consulta->fetch(PDO::FETCH_ASSOC);
		return $qtd['qtd'];
	}
	
	public function listSubCategoria(){
		$sql = "SELECT * FROM  categoria WHERE  categoria_self_join is not null";
		$consulta = DB::prepare($sql);
		$consulta->execute();
		return $consulta->fetchAll(PDO::FETCH_ASSOC);		
	}
}