<? 
	require 'Slim/Slim.php';
	\Slim\Slim::registerAutoloader();
	$app = new \Slim\Slim(array(
		'debug' => false
	));
	
	$app->get('/',function(){
		echo "oi";
	});
	
	$app->get('/:controller/:action(/:parameter)', function($controller,$action,$parametro = null){
		include_once "app/model/{$controller}.php";
		$class = new $controller();
		$chamada = call_user_func_array(array($class, $action), array($parametro));
		header("Content-Type: application/json");
		echo json_encode($chamada);
	});

	$app->post('/:controller/:action', function($controller,$action) use($app){
		//$app->response()->header("Content-Type", "application/json");
		$request = $app->request()->post();
		//var_dump($request);
		include_once "app/model/{$controller}.php";
		$class = new $controller();
		$chamada = call_user_func_array(array($class, $action), array($request));
		header("Access-Control-Allow-Origin: *");
		header('Content-Type: application/json');
		header('Access-Control-Allow-Methods: POST');
		header('Accept: application/json');
		echo json_encode($chamada);
	});
	
	$app->put('/:controller/:action(/:parameter)', function($controller,$action,$parametro = null){
		include_once "app/model/{$controller}.php";
		$class = new $controller();
		$chamada = call_user_func_array(array($class, $action), array($parametro));
	});
	
	$app->error(function(Exception $e) use ($app){
		echo $e->getMessage()."<br/>";
		echo $e->getFile()."<br/>";
		echo $e->getLine();
	});
	
	$app->run();
?>