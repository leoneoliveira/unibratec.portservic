<?
		try{
			$mUploadDir = "../web/uploadFile/";
			$data['debug'] = is_dir($mUploadDir);
			$data['mydir'] = $_SERVER['PHP_SELF'];
			$array_dir = explode("/",$data['mydir']);
			$id = end($array_dir);
			foreach($_FILES as $mFile) {
				if (!empty($mFile["size"])) {
					// Convert bytes to megabytes
					$fileSize = ($mFile["size"] / 1024) / 1024;
					
					if ($fileSize > 2) {
						$data["success"] = false;
						$data["error"] = "Arquivo maior do que 2 MB";
						return $data;
						//throw new Exception("Arquivo maior do que 2 MB");
					}
				} else {
					$data["success"] = false;
					$data["error"] = "Arquivo maior do que 2 MB ou corrompido";
					return $data;
					//throw new Exception("Arquivo maior do que 2 MB ou corrompido");
				}
				$extencions = array("jpg","gif","png");
				$type = explode(".",$mFile["name"]);
				$name = $type[0]."_".$id.".".$type[1];
				if(move_uploaded_file($mFile["tmp_name"], $mUploadDir.$name)) {
					//$sql = "update usuario set foto = :foto where id = :id;";
					//$insert = DB::prepare($sql);
					//$insert->bindParam(":foto",$name);
					//$insert->bindParam(":id",$request['id']);
					//$insert->execute();
					$data["success"] = true;
					$data["ServicoImagem"] = $_FILES;
				}else{
					$data["success"] = false;
					$data["error"] = "diretorio não existe";
				}
			}

		}catch(Exception $e){
			$data["success"] = false;
			$data["error"] = $e->getMessage();
		}
		
		header("Access-Control-Allow-Origin: *");
		header('Content-Type: application/json');
		header('Access-Control-Allow-Methods: POST');
		header('Accept: application/json');
		echo json_encode($data);