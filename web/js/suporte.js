var suporte = {
	construct:$(function(){
		suporte.enviar();
	}),
	enviar:function(){
		$(".enviar_suporte").click(function(){
			var dados = $("#form_servico").serialize();
			if(
				$("#assunto").val() != ""
				&&
				$("#descricao").val() != ""
			){
				$.ajax({
					url:"http://portservise.esy.es/portservise_webservice/SuporteModel/insertSuporte",
					data:dados,
					type:"POST",
					dataType:"JSON",
					success:function(data){
						if(data.success){
							$("#aviso_suporte").css({"display":"none"});
							$("#suporte_ok").css({"display":"block"});
							$("#span_suporte_ok").html("Enviado com sucesso.");
						}
					}
				});
			}else{
				$("#suporte_ok").css({"display":"none"});
				$("#aviso_suporte").css({"display":"block"});
				$("#aviso_span_suporte").html("É necessario Preencher todos os campos");
			}
		});
	}
}

suporte.construct;