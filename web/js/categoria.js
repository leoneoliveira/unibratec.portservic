var categoria = {
	construct:$(function(){
		categoria.listMainCategorias();
		categoria.indexMainList();
	}),
	listMainCategorias:function(){
		$.ajax({
			url:"http://portservise.esy.es/portservise_webservice/CategoriaModel/comboBox",
			type:"POST",
			dataType:"JSON",
			success:function(data){
				//console.log(data);
				for(var i = 0; i < data.length; i++){
					$("#categoria_id").append("<optgroup label='"+data[i].categoria_descricao+"'>");
					//console.log(data.[i].categoria_subs);
					for(var j = 0; j < data[i].categoria_subs.length; j++){
						$("#categoria_id").append("<option value="+data[i].categoria_subs[j].categoria_id+">&nbsp;&nbsp;&nbsp;"+data[i].categoria_subs[j].categoria_descricao+"</option>");
					}
					$("#categoria_id").append("</optgroup>");
				}
			},
			error:function(data){
				
			}
		});
	},
	indexMainList:function(){
		$.ajax({
			url:"http://portservise.esy.es/portservise_webservice/CategoriaModel/listMainCategorias",
			type:"POST",
			dataType:"JSON",
			success:function(data){
				console.log(data);
				for(var i = 0; i < data.length; i++){
					$("#dropdown-menu").append("<li><a href='#'>"+data[i].categoria_descricao+"</a></li>");
				}
			},
			error:function(data){
				
			}
		});		
	},	
	detalheCategoria:function(self_join){
		$.ajax({
			url:"http://portservise.esy.es/portservise_webservice/CategoriaModel/listCategoriasById",
			type:"POST",
			dataType:"JSON",
			data:{codcategoria:self_join},
			success:function(data){
				$("#menu_link_categoria").html(data[0].categoria_descricao);
				console.log(data);
			}
		});
	},
	listFiltroCategorias:function(){
		$.ajax({
			url:"http://portservise.esy.es/portservise_webservice/CategoriaModel/comboBox",
			type:"POST",
			dataType:"JSON",
			success:function(data){
				//console.log(data);
				for(var i = 0; i < data.length; i++){
					$("#categoria_filtro_id").append("<option value="+data[i].categoria_id+" >&nbsp;&nbsp;&nbsp;"+data[i].categoria_descricao+"</option>");
					//console.log(data.[i].categoria_subs);
					for(var j = 0; j < data[i].categoria_subs.length; j++){
						$("#categoria_filtro_id").append("<option value="+data[i].categoria_subs[j].categoria_id+">&nbsp;&nbsp;&nbsp;"+data[i].categoria_subs[j].categoria_descricao+"</option>");
					}
					///$("#categoria_id").append("</optgroup>");
				}
			},
			error:function(data){
				
			}
		});
	}
	
}

categoria.construct;