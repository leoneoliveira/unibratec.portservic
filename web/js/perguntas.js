var perguntas = {
	construct:$(function(){
		perguntas.enviar();
	}),
	enviar:function(){
		$("#enviar_pergunta").click(function(){
			var dados = $("#mandar_pergunta").serialize();
			var idPrestador = $('#prestador_id').val();
			var descricao_prest = $('#descricao_prest').val();
			var pergunta = $('#pergunta').val();
			var tipo = 'P';
			

			$.ajax({
				url:"http://portservise.esy.es/portservise_webservice/PerguntasRespostas/manterPergunta",
				data:dados,
				type:"POST",
				dataType:"JSON",
				success:function(data){
					console.log(data);
					$('#msg_enviada').hide();
					$('#enviar_pergunta').hide();					
					$('#msg_resposta').html('<div class="alert alert-info" role="alert">Sua pergunta foi enviada para o prestador, aguarde até que ele responda</div>');
							// Inicio: Enviar Email Com pergunta
							$.ajax({
								url:"http://portservise.esy.es/portservise_webservice/UsuarioModel/listUsuariosById",
								data:{id:idPrestador},
								type:"POST",
								dataType:"JSON",
								success:function(data){
									objUsuario = data[0];
										
										// Inicio: Enviando email para o prestador
										$.ajax({
										url:"../web/util/emailPerguntaResposta.php",
										data:{email:data[0].email,descricao_prest:descricao_prest,pergunta:pergunta,tipo:tipo },
										type:"POST",
										//dataType:"JSON",
										success:function(data){
											console.log(data)
										},
										error:function(data){
											alert('Erro 3' + data)					
										}
										});
										// Fim: Enviando email para o prestador

								},
								error:function(data){
									alert('Erro 2' + data)					
								}
							});
							// Fim: Enviar Email Com pergunta

				},
				error:function(data){
					alert('Erro 1' + data)	
					$('#msg_resposta').html('<div class="alert alert-danger" role="alert">Sua Messagem não foi enviado, por favor tente mais tarde.</div>');

				}
			});

		});
	}
}

perguntas.construct;