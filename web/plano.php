<?php 
    include 'verificar.php';
    include 'head.php'; ?>
<body>
    <?php include 'nav_top.php'; ?>
    <hr class="topbar"/>
    <div class="container">
        <div class="row">
            <?php include "menu-dashboard.php" ?>
            <div class="col-sm-9">
                <div class="panel panel-default">

                    <div class="panel-heading">Meu Plano</div>
                    <div class="panel-body">
                        <br />
                        <div class="row">
                            <div class="alert alert-danger" id="alerta-plano-erro" style="display:none;">
                                <strong>Alerta!</strong>
                                <span id="msgAlerta"></span>
                            </div>
                           
                            <div class="alert alert-success" id="alerta-plano-sucesso" style="display:none;">
                                <strong>Tudo certo!</strong>
                                <span id="msgok"></span>
                            </div>
                        </div>
                        <form class="form-vertical" id="form_plano">
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-12 ">
                                    <div class="form-group">
                                        <h1><small>Seu plano Atual é: </small> <span id="nm_plano_usuario"></span> </h1>
                                        <p style="text-align: justify;" id="ds_plano_usuario"></p>
                                    </div>
                                    <hr>
                                    <br/>
                                    <div class="form-group">
                                        <label for="codplano">Para mudar de plano selecione um abaixo</label>
                                        <select class="form-control" name="codplano" id="codplano">
                                            <option value="">Selecione o plano</option>
                                        </select>
                                    </div>
                                    <div class="controleDetalhe">
                                        <div class="form-group">
                                            <h1><small> Valor: </small> <span id="nm_plano" style="color: darkgoldenrod;"> </span> </h1>
                                            <p style="text-align: justify;" id="ds_plano"></p>
                                        </div>
                                        <div class="form-group">
                                            <label for="dias">Dias</label>
                                            <input type="text" class="form-control" maxlenght="4" onkeypress="mascara(this,masknumber)" name="dias" id="dias" style="width: 80px;"/>
                                        </div>
                                        <div id="dados_pagamento_caixa">
                                            <div class="form-group">
                                                <label>Selecione uma forma de pagamento:</label>
                                            </div>  
                                            <div class="form-group">
                                                <input type="hidden" name="bandeira" value="">
                                                <li style="list-style: none;display: inline; float: left; position:relative; width: 65px; ">
                                                    <input type="radio" name="codformapagamento" value="1" id="visa_credito">
                                                    <label tipo="credito" for="visa_credito" formapagamento="credito" bandeira="visa">
                                                        <img src="img/visa.png" alt="">
                                                    </label>
                                                </li>
                                                <li style="list-style: none;display: inline; float: left; padding-left: 10px; position:relative; width: 65px; ">
                                                    <input type="radio" name="codformapagamento" value="1" id="master_credito">
                                                    <label tipo="credito" for="master_credito" formapagamento="credito" bandeira="mastercard">
                                                        <img src="img/master.png" alt="">
                                                    </label>
                                                </li >
                                                <li style="list-style: none;display: inline; float: left; padding-left: 10px; position:relative; width: 65px; ">
                                                    <input type="radio" name="codformapagamento" value="1" id="diners_credito">
                                                    <label tipo="credito" for="diners_credito" formapagamento="credito" bandeira="diners">
                                                        <img src="img/diners.png" alt="">
                                                    </label>
                                                </li>
                                                <li style="list-style: none;display: inline; float: left; padding-left: 10px; position:relative; width: 65px; ">
                                                    <input type="radio" name="codformapagamento" value="1" id="amex_credito">
                                                    <label tipo="credito" for="amex_credito" formapagamento="credito" bandeira="amex">
                                                        <img src="img/amex.png" alt="">
                                                    </label>
                                                </li>
                                                <li style="list-style: none;display: inline; float: left; padding-left: 10px; position:relative; width: 65px; ">
                                                    <input type="radio" name="codformapagamento" value="3" id="boleto">
                                                    <label tipo="boleto" for="boleto" formapagamento="boleto">
                                                        <img src="img/boleto.png" alt="">
                                                    </label>
                                                </li>
                                            </div>
                                            <div class="clearfix"></div>
                                            <br/>
                                            <div class="form-group cartao" style="display:none;">
                                                <label>N&uacute;mero:</label>
                                                <input type="text" class="form-control" name="numerocartao" id="numerocartao" onkeypress="mascara(this,maskcartao)" maxlength="19"/>
                                            </div>
                                            <div class="form-group cartao" style="display:none;">
                                                <label> CVV:</label>
                                                <input type="text" class="form-control" style="width:80px;" name="codigoseguranca" id="codigoseguranca" maxlength="5"/>
                                            </div>
                                            <div class="form-group cartao" style="display:none;">
                                                <label>Nome</label>
                                                <input type="text" class="form-control" name="nomecartao" id="nomecartao" placeholder="Nome igual ao que estar no cartão"/>
                                            </div>
                                            <div class="form-group cartao" style="display:none;">
                                                <label>Validade:</label><br/>
                                                <select class="form-control" name="mes" id="mes" style="width: 120px; display: inline;">
                                                    <option value="">M&ecirc;s</option>
                                                    <option value="1">01</option>
                                                    <option value="2">02</option>
                                                    <option value="3">03</option>
                                                    <option value="4">04</option>
                                                    <option value="5">05</option>
                                                    <option value="6">06</option>
                                                    <option value="7">07</option>
                                                    <option value="8">08</option>
                                                    <option value="9">09</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                               
                                                <select class="form-control" name="ano" id="ano" style="width: 120px; display: inline;">
                                                    <option value="">Ano</option>
                                                    <?php for ($i = date("Y"); $i <= date("Y") + 8; $i++) {  ?>
                                                    <option value="<?php print($i) ?>"><?php print($i) ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group cartao" style="display:none;">
                                                <label>Pacelas</label><br/>
                                                <input type="text" class="form-control" onkeypress="mascara(this,masknumber)" name="parcelas" id="parcelas" style="width: 80px; display: inline;"/>
                                                <input type="text" class="form-control" readonly name="valorparcelas" id="valorparcelas" style="width: 160px; display: inline;"/>
                                            </div>
                                        <br/>
                                        <input type="hidden" name="codusuario" id="codusuario" value="<?php echo $_SESSION['usuario']['id'];?>">
                                        <input type="hidden" name="valor" id="valor">
                                        <input type="hidden" name="valorbase" id="valorbase">
                                        <input type="hidden" name="qtdanuncio" id="qtdanuncio">
                                        <input type="hidden" name="data" id="data" value="<?php echo date('Ymd'); ?>">
                                        <div class="form-group">
                                            <a class="btn btn-primary aderirPlano"><i class="icon-ok"></i>  Pagar</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
    <br />
</div>
</div>
<script src="js/plano.js"></script>
<script src="util/util.js"></script>
<script src="js/jquery.validate.min.js" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(".controleDetalhe").hide();
    plano.usuarioListPlanosById(<?php echo $_SESSION['usuario']['plano_id']; ?>);

    $(document).ready(function(){
       jQuery('#codplano').on('change',function(){
            if(jQuery(this).val() != ""){
                plano.detalhePlanos(jQuery(this).val());
                jQuery(".controleDetalhe").show();
            }else{
                jQuery(".controleDetalhe").hide();
            }
       });

       jQuery("#dias").on("keyup", function(){
            $("#alerta-plano-erro").css({"display":"none"});
            $(this).css({"border-color":"#D9D9D9"});
            if($(this).val() != "" && $(this).val() > 30){
                var dias = Math.ceil (( $(this).val() - 30) * 100 / $("#valorbase").val());
                var valorfinal = parseFloat(dias) + parseFloat($("#valorbase").val());
                $("#valor").val(valorfinal); 
                $("#nm_plano").html(" R$ "+valorfinal);
            }else if($(this).val() == 30){
                $("#valor").val($("#valorbase").val()); 
                $("#nm_plano").html(" R$ "+$("#valorbase").val());
            }else{
                $(this).css({"border-color":"red"});
                $("#alerta-plano-erro").css({"display":"block"});
                $("#msgAlerta").html("O numero de parcela deve ser maior ou igual a 30!");  
            }
       });

       jQuery(".aderirPlano").on("click", function(){
            if($("#form_plano").valid() === true){
                plano.aderirPlano();
            }
       });

        $("body").on("click", "label[tipo='credito']",  function() {
        
            $("label[tipo='cheque']").removeClass('ativo');
            $("label[tipo='dinheiro']").removeClass('ativo');
            $("label[tipo='credito']").removeClass('ativo');
            $("label[tipo='boleto']").removeClass('ativo');
            $("label[tipo='debito']").removeClass('ativo');
            $(this).addClass('ativo');
            var bandeira = $(this).attr('bandeira');
            $("input[name='bandeira']").val(bandeira);

            var formapagamento = $(this).attr('formapagamento');
            $("input[name='formapagamento']").val(formapagamento);
            $(".cartao").show(100);
            $(".cheque").hide(100);
        });
        $("body").on("click", "label[tipo='boleto']", function() {

            $("label[tipo='cheque']").removeClass('ativo');
            $("label[tipo='dinheiro']").removeClass('ativo');
            $("label[tipo='credito']").removeClass('ativo');
            $("label[tipo='boleto']").removeClass('ativo');
            $("label[tipo='debito']").removeClass('ativo');
            $(this).addClass('ativo');
            var bandeira = $(this).attr('bandeira');
            $("input[name='bandeira']").val(bandeira);

            var formapagamento = $(this).attr('formapagamento');
            $("input[name='formapagamento']").val(formapagamento);
            $(".cartao").hide(100);
            $(".cheque").hide(100);
        });

        jQuery("#parcelas").on("keyup", function(){
            $("#alerta-plano-erro").css({"display":"none"});
            $("#parcelas").css({"border-color":"#D9D9D9"});
            if(jQuery(this).val() != "" && jQuery.isNumeric($(this).val())){
                if(jQuery(this).val() <= 10){
                    var valor = jQuery("#valor").val() / jQuery(this).val();
                    jQuery("#valorparcelas").val(" "+jQuery(this).val()+"x de R$ "+valor);
                }else{
                    $("#parcelas").css({"border-color":"red"});
                    $("#alerta-plano-erro").css({"display":"block"});
                    $("#msgAlerta").html("O numero de parcela deve ser menor ou igual a 10!");     
                }
            }
        });

        $("#form_plano").validate({
            focusInvalid: true, 
            rules: {
                numerocartao:{
                    required: function(){
                        if ($("input[name='codformapagamento']:checked").val() != 2 && $("input[name='codformapagamento']:checked").val() != 3){
                            return true;
                        }
                        else{
                            return false;
                        }
                    },
                },
                nomecartao:{
                    required: function(){
                        if ($("input[name='codformapagamento']:checked").val() != 2 && $("input[name='codformapagamento']:checked").val() != 3){
                            return true;
                        }
                        else{
                            return false;
                        }
                    },
                },
                parcelas:{
                    required: function(){
                        if ($("input[name='codformapagamento']:checked").val() != 2 && $("input[name='codformapagamento']:checked").val() != 3){
                            return true;
                        }
                        else{
                            return false;
                        }
                    },
                },
                codigoseguranca:{
                    required: function(){
                        if ($("input[name='codformapagamento']:checked").val() != 2 && $("input[name='codformapagamento']:checked").val() != 3){
                            return true;
                        }
                        else{
                            return false;
                        }
                    },
                },
                mes:{
                    required: function(){
                        if ($("input[name='codformapagamento']:checked").val() != 2 && $("input[name='codformapagamento']:checked").val() != 3){
                            return true;
                        }
                        else{
                            return false;
                        }
                    },
                },
                ano:{
                    required: function(){
                        if ($("input[name='codformapagamento']:checked").val() != 2 && $("input[name='codformapagamento']:checked").val() != 3){
                            return true;
                        }
                        else{
                            return false;
                        }
                    },
                },
            },

            invalidHandler: function (event, validator) {              
                $('#alerta-plano-erro').text('Os campos marcados são de preenchimento obrigatório');
                $('#alerta-plano-erro').show();
                $('#alert-warning').hide();
                $('#alert-success').hide();
            },

            errorPlacement: function (error, element) { 
                
            },

            highlight: function (element) { 
                $(element).css({"border-color":"red"});
            },

            unhighlight: function (element) {
                
            },

            success: function (label, element) {
                $(element).css({"border-color":"#D9D9D9"}); 
            },

            submitHandler: function (form) {
                //form.submit();
            }
        });
    });
</script>
<?php include 'footer.php'; ?>
</body>
<!-- Mirrored from templates.expresspixel.com/bootlistings/account_account.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 25 Feb 2016 12:17:51 GMT -->
</html>