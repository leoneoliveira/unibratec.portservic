<div class="col-sm-3">
    <div class="sidebar-account">
        <div class="row ">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Minha conta</div>
                    <div class="panel-body">
                        <ul class="nav">
                            <li>
                                <a class="active" href="dashboard.php">Painel de controle</a>
                            </li>
                            <li>
                                <a class="active" href="perfil.php">Meu perfil</a>
                            </li>
                            <li>
                                <a class="active" href="conta.php">Alterar senha</a>
                            </li>
                            <li>
                                <a class="active" href="meus_anuncios.php">Gerenciar anúncios</a>
                            </li>
                            <li>
                                <a class="active" href="novo-anuncio.php">Criar novo anúncio</a>
							</li>
                            <li>
                                <a class="active" href="suporte.php">Suporte</a>
							</li>							
							<li>
                                <a class="active" href="sair.php">Sair</a>
							</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row hidden-xs">
            <div class="col-lg-12">
                <div class="well">
                    <div class="row ">
                        <div class="col-lg-3">
                            <img src="css/images/icons/Crest.png" width="45"/>
                        </div>
                        <div class="col-lg-9">
                            <h4 style="margin-top: 0">Aumentar a visibilidade</h4>
                            <p>Não se esqueça de pedir feedback a sua lista para ganhar mais visibilidade</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>