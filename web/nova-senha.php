<?php include 'head.php'; ?>
<body>
    <?php include 'nav_top.php';?>
    <hr class="topbar"/>
    <div class="container">
        <?php
        if (isset($_GET['hash'])) {
            $hash =  utf8_decode($_GET['hash']);
        }else{
            $hash = '';
        }
        ?>
        <br />
        <div class="row">
            <div class="col-sm-12">
                <h1>Nova senha</h1>
                <hr />
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <form class="form-vertical">
                            <fieldset>
                                
                                <div class="alert alert-danger" id="danger_nv_senha" style="display:none;">
                                    <strong>Alerta!</strong>
                                    <span id="span_vn_senha"></span>
                                </div>

                                <div class="alert alert-info" id="esqueciNova_ok" style="display:none;">
                                    <p>Senha alterada com sucesso. Vocês sera redirecionado para sua conta.</p>
                                </div>
                                
                                <div class="row">
                                    <div class="col-sm-12" >
                                        <form class="form-vertical" id="leonePorra">
                                            <div class="well">
                                                <div class="form-group">
                                                    <label for="nome">Email</label>
                                                    <input type="email" class="form-control " id="emailEsqNova" name="email" placeholder="Confirme seu email">
                                                    
                                                </div>
                                                <div class="form-group">
                                                    <label for="email">Nova senha</label>
                                                    <input type="password" class="form-control " id="senhaEsqNova" placeholder="Informe a nova senha">
                                                </div>
                                                <div class="form-group">
                                                    <label for="senha">Repetir nova senha</label>
                                                    <input type="password" class="form-control" id="senhaEsqNovaRep" name="senha" placeholder="Confirme a nova senha">
                                                </div>
                                                <br />
                                                <input type="hidden" name="id" id="idUser" value="<?php echo  $hash; ?>">
                                                <input id="esquiciSenhaNova" type="button" value="Alterar senha" class="btn btn-primary">
                                            </div>
                                        </div>
                                    </form>
                                    
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 account-sidebar hidden-sm">
                                <div class="row">
                                    <div class="col-sm-3" style="text-align: center;">
                                        <img src="css/images/icons/Crest.png" width="50"/>
                                    </div>
                                    <div class="col-sm-8">
                                        <h3>Por que nós?</h3>
                                        <p>Nós somos uma das empresas mais conhecidas, atraindo milhares de prestadores de serviço a cada mês.<p>
                                        </div>
                                    </div>
                                    <br />
                                    <div class="row">
                                        <div class="col-sm-3" style="text-align: center;">
                                            <img src="css/images/icons/Pie-Chart.png" width="40"/>
                                        </div>
                                        <div class="col-sm-8">
                                            <h3>Ímã para os compradores</h3>
                                            <p>Temos certeza de suas listas receber a exposição máxima e é apresentado de uma forma atrativa</p>
                                        </div>
                                    </div>
                                    <br />
                                    <div class="row">
                                        <div class="col-sm-3" style="text-align: center;">
                                            <img src="css/images/icons/Search.png" width="40"/>
                                        </div>
                                        <div class="col-sm-8">
                                            <h3>Pesquisas focalizadas</h3>
                                            <p>Nossa tecnologia e algoritmo corresponde potenciais servidores diretamente para a sua lista</p>
                                        </div>
                                    </div>
                                    <br />
                                    <div class="row">
                                        <div class="col-sm-3" style="text-align: center;">
                                            <img src="css/images/icons/Telephone.png" width="40"/>
                                        </div>
                                        <div class="col-sm-8">
                                            <h3>Web Móvel</h3>
                                            <p>Suas listagens será sempre acessível a todos, mesmo quando estão em movimento, através do nosso site para celular responsiva</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div><!-- Modal -->
    
    <script src="js/jquery.js"></script>
    <script src="js/usuario.js"></script>
    <?php include 'footer.php'; ?>
</body>
<!-- Mirrored from templates.expresspixel.com/bootlistings/register.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 25 Feb 2016 10:59:57 GMT -->
</html>