<?php include 'head.php'; ?>
<body>
    <?php 
	include 'nav_top.php'; 
    include 'verificar.php';
	?>
	
    <hr class="topbar"/>
    <div class="container">
        <div class="row">
            <?php include "menu-dashboard.php" ?>
            <div class="col-sm-9">
				<div id="aviso_servico" style="display:none;" class="alert alert-danger">
				  <strong>Aviso</strong>
				  <span id="aviso_span_servico"></span>
				</div>
                <form  id="form_servico" class="form-vertical">
                    <div class="panel panel-default">
                        <div class="panel-heading">Escolha a categoria</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-12 "  >
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-2" style="margin-top: 10px;">
                                                <label>Categoria</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <select id="categoria_id" class="form-control " name="categoria_id">
                                                    <option value="">escolha a categoria</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">Detalhes do anúncio</div>
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label>Título </label>
                                        <input type="text" id="titulo" name="titulo" class="form-control " >
                                    </div>
                                    <div class="col-sm-12"><br />
                                        <label>Descrição </label>
                                        <textarea id="descricao" name="descricao" class="form-control col-sm-8 expand" rows="6" style="width: 99%"></textarea>
                                    </div>
                                    <div class="col-sm-12"><br />
                                        <label>Palavras-chave</label>
                                        <input id="palavra_chave" name="palavra_chave" type="text" class="form-control " >
                                    </div>
									<div class="col-sm-12"><br />
                                        <label>Tempo de experiencia</label>
                                        <select name="experiencia" id="experiencia" class="form-control ">
											<option value="0">0 a 1 ano</option>
											<option value="1">1 a 3 anos</option>
											<option value="2">mais de 3 anos</option>
                                        </select>
                                    </div>
									<div class="col-sm-12"><br />
                                        <label>Valor</label>
                                        <input id="valor" name="valor" type="text" class="form-control " >
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">Informações do anúncio</div>
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>CEP</label>
                                        <input id="cep" name="cep" type="text" class="form-control "  >
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Cidade</label>
                                        <input id="cidade" name="cidade" type="text" class="form-control "  >
                                    </div>
                                    <div class="col-sm-6"><br />
                                        <label>Estado</label>
										<select name="uf" id="uf" class="form-control ">
											<option value="">Selecione</option>
											<option value="AC">Acre</option>
											<option value="AL">Alagoas</option>
											<option value="AP">Amapá</option>
											<option value="AM">Amazonas</option>
											<option value="BA">Bahia</option>
											<option value="CE">Ceará</option>
											<option value="DF">Distrito Federal</option>
											<option value="ES">Espirito Santo</option>
											<option value="GO">Goiás</option>
											<option value="MA">Maranhão</option>
											<option value="MT">Mato Grosso</option>
											<option value="MS">Mato Grosso do Sul</option>
											<option value="MG">Minas Gerais</option>
											<option value="PA">Pará</option>
											<option value="PB">Paraiba</option>
											<option value="PR">Paraná</option>
											<option value="PE">Pernambuco</option>
											<option value="PI">Piauí</option>
											<option value="RJ">Rio de Janeiro</option>
											<option value="RN">Rio Grande do Norte</option>
											<option value="RS">Rio Grande do Sul</option>
											<option value="RO">Rondônia</option>
											<option value="RR">Roraima</option>
											<option value="SC">Santa Catarina</option>
											<option value="SP">São Paulo</option>
											<option value="SE">Sergipe</option>
											<option value="TO">Tocantis</option>
										 </select>
                                    </div>
                                    <div class="col-sm-6"><br />
                                        <label>Rua</label>
										<input id="endereco" name="endereco" type="text" class="form-control "  >
									</div>
									<div class="col-sm-6">
                                        <label>Bairro</label>
                                        <input id="bairro" name="bairro" type="text" class="form-control "  >
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="privacidade" id="privacidade" value="1">
                                                Não mostrar meu endereço publicamente
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">Adicionar imagens</div> 
                        <div class="panel-body">
                            <h3>Um anúncio com fotos é visto até 7 vezes mais que um anúncio sem foto.</h3>
                            <div id="my-dropzone" action="http://portservise.esy.es/portservise_webservice/uploadCustomer.php/ServicoImagenModel/uploadServico/<?= $_SESSION['usuario']['id'];?>" class="dropzone"></div>
                            <br /><p><small>* por favor note que as imagens exibidas são cortadas / redimensionada apenas para fins de exibição</small></p>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">Anúncio completo</div>
                        <div class="panel-body">
                            <div class="checkbox">
                                <label>
                                    <input id="aceita_termos" name="aceita_termos" type="checkbox" value="1" /> Eu concordo com os termos e condições e regulamentos.
                                </label>
                            </div>
                            <br />
                            <input type="button" class="btn btn-default hidden-xs cadastrar_serico" value="Guardar rascunho" />
                            <input type="button" class="btn btn-default hidden-xs" value="Visualizar anúncios"/>
                            <a class="btn btn-primary pull-right cadastrar_serico"><i class="icon-ok"></i>  Publicar anúncio</a>
							<input type="hidden" name="usuario_id" id="usuario_id" value="<?= $_SESSION['usuario']['id'];?>"/>
                            <input type="hidden" name="status" id="status" value="P"/>
                            <br>
                            <br /><p class=" hidden-xs" style="text-align: right"><small>* Seu anúncio passará por uma avalição tecnica para ser publicado </small></p>
                        </div>
                    </div>
					<div class="panel panel-default">
						<select style="display:none;" id="fotos" multiple></select>
					</div>
                </div>
            </div>
        </div>
    </div>
</form>
<br />
</div>
</div>
<script src="js/categoria.js"></script>
<script src="js/mascara.js"></script>
<script src="js/servico.js"></script>
<script src="js/jquery.mask.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#cep").mask("99999-999");
    $('input[name=valor]').mask('#.##0,00', {reverse: true, maxlength: false});
});
</script>
<?php include 'footer.php'; ?>
</body>
<!-- Mirrored from templates.expresspixel.com/bootlistings/account_ad_create.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 25 Feb 2016 12:16:26 GMT -->
</html>