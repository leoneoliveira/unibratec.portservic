<?php include 'head.php'; ?>
<body>
    <?php 
	include 'nav_top.php'; 
    include 'verificar.php';
	?>
	
    <hr class="topbar"/>
    <div class="container">
        <div class="row">
            <?php include "menu-dashboard.php" ?>
            <div class="col-sm-9">
				<div id="suporte_ok" class="alert alert-info" style="display:none;">
					<strong>Sucesso!</strong> 
					<span id="span_suporte_ok"></span>
				</div>
				<div id="aviso_suporte" style="display:none;" class="alert alert-danger">
				  <strong>Aviso</strong>
				  <span id="aviso_span_suporte"></span>
				</div>
                <form  id="form_servico" class="form-vertical">
                    <div class="panel panel-default">
                        <div class="panel-heading">Suporte</div>
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label>Assunto</label>
                                        <input type="text" id="assunto" name="assunto" class="form-control " >
                                    </div>
                                    <div class="col-sm-12"><br />
                                        <label>Descrição </label>
                                        <textarea id="descricao" name="descricao" class="form-control col-sm-8 expand" rows="6" style="width: 99%"></textarea>
                                    </div>
									<!--<div class="col-sm-12"><br/>
										<label>Adicionar imagens</label>
										<div id="my-dropzone" action="http://portservise.esy.es/portservise_webservice/uploadCustomer.php/ServicoImagenModel/uploadServico/<?= $_SESSION['usuario']['id'];?>" class="dropzone"></div>
										<br /><p><small>* por favor note que as imagens exibidas são cortadas / redimensionada apenas para fins de exibição</small></p>
									</div>-->
									<div class="col-sm-12"><br/>
										<a class="btn btn-primary pull-right enviar_suporte"><i class="icon-ok"></i>  Enviar</a>
									</div>
                                </div>
                            </div>
                        </div>
						<input type="hidden" name="id_usuario" id="id_usuario" value="<?= $_SESSION['usuario']['id'];?>" />
						<div class="panel panel-default">
							<select style="display:none;" id="fotos" multiple></select>
						</div>
                    </div>
                </form>
            </div>
        </div>
    </div>
	<script src="js/suporte.js"></script>
<br />
<?php include 'footer.php'; ?>
</body>
<!-- Mirrored from templates.expresspixel.com/bootlistings/account_ad_create.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 25 Feb 2016 12:16:26 GMT -->
</html>