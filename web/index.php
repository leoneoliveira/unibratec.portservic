<?php include 'head.php'; ?>
<?
session_start();
if(isset($_SESSION['usuario'])){
?>

<?
}
?>
<body>
    <?php include 'nav_top.php'; ?>
    <div class="jumbotron home-search" style="">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <br />
                    <p class="main_description">
                        Pesquise milhares de prestadores, serviços e qualidade em um só lugar</p>
                        <br /><br />
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-2" style="text-align: center">
                                <div class="row">
                                    <div class="col-sm-8 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon input-group-addon-text">Encontre-me um</span>
                                            <input type="text" class="form-control col-sm-3" placeholder="Pedreiro, Chaveiro, Mecanico, Pintor ">
                                            <div class=" input-group-addon hidden-xs">
                                                <div class="btn-group" >
                                                    <button type="button" class="btn  dropdown-toggle" data-toggle="dropdown">
                                                        Categorias <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu" id="dropdown-menu" role="menu">
                                                    <!--<li><a href="#">Cars, Vans & Motorbikes</a></li>
                                                    <li><a href="#">Community</a></li>
                                                    <li><a href="#">Flats & Houses</a></li>
                                                    <li><a href="#">For Sale</a></li>
                                                    <li><a href="#">Jobs</a></li>
                                                    <li><a href="#">Pets</a></li>
                                                    <li><a href="#">Services</a></li>-->
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br />
                    <br />
                    <div class="row">
                        <div class="col-sm-12" style="text-align: center">
                            <a href="anuncios.php" class="btn btn-primary search-btn">Pesquisar</a>
                        </div>
                    </div>
                    <br />
                    <br />
                    <div class="row">
                        <div class="col-sm-12" style="text-align: center">
                            <div id="quotes">
                                <div class="text-item" style="display: none;">O carro quebrou e está <strong>parado no meio da rua</strong>? Chame o <strong>Bruno</strong>, mecânico, ele pode te ajudar.</div>
                                <div class="text-item" style="display: none;">Perdeu a <strong>chave de casa</strong> e não sabe para quem ligar? Ligue para o <strong>Hugo</strong>, chaveiro, ele pode te atender.</div>
                                <div class="text-item" style="display: none;">Precisando <strong>reformar o seu apartamento</strong>? Ligue para o <strong>Carlos e Fábio</strong>, pedreiro e pintor de excelente qualidade.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-8">
                <div class="row directory">
                    <div class="col-sm-12 ">
                        <h2><span>Serviços disponíveis</span></h2>
                    </div>
                </div>
                <div class="row directory">
                    <div class="col-xs-12">
                        <div class="directory-block col-sm-4 col-xs-6">
                            <div class="row">
                                <div class="col-sm-3">
                                    <i class="fa fa-home"></i>
                                </div>
                                <div class="col-sm-9">
                                    <h4>Casa</h4>
                                    <p><a href="listings.html" >Hidráulico</a>, <a href="listings.html" >Marceneiro</a>, <a href="listings.html" >Pintor</a>, <a href="listings.html">Pedreiro</a>, <a href="listings.html" >Eletricista</a>, <a href="listings.html" >Arquiteto</a>, <a href="listings.html" >Dedetizador</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="directory-block col-sm-4 col-xs-6">
                            <div class="row">
                                <div class="col-sm-3">
                                    <i class="fa fa-car"></i>
                                </div>
                                <div class="col-sm-9">
                                    <h4>Carro</h4>
                                    <p><a href="listings.html" >Mecânico</a>, <a href="listings.html" >Funilaria</a>, <a href="listings.html" >Eletricista</a><a href="listings.html" >Ar Condicionado</a>, <a href="listings.html" >Alarme</a>, <a href="listings.html" >Inspeção Veicular</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="directory-block col-sm-4 col-xs-6">
                            <div class="row">
                                <div class="col-sm-3">
                                    <i class="fa fa-scissors"></i>
                                </div>
                                <div class="col-sm-9">
                                    <h4>Moda e Beleza</h4>
                                    <p><a href="listings.html" >Esteticista</a>, <a href="listings.html" >Manicure</a>, <a href="listings.html" >Pedicure</a>, <a href="listings.html">Cabeleireiro</a>, <a href="listings.html" >Costura</a>, <a href="listings.html" >Depilação</a>, <a href="listings.html" >Maquiador</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="directory-block col-sm-4 col-xs-6">
                            <div class="row">
                                <div class="col-sm-3">
                                    <i class="fa fa-briefcase"></i>
                                </div>
                                <div class="col-sm-9">
                                    <h4>Consultoria</h4>
                                    <p><a href="listings.html" >Advogado</a>, <a href="listings.html" >Contador</a>,<a href="listings.html" >Economia e Finanças</a>, <a href="listings.html" >Segurança do Trabalho</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="directory-block col-sm-4 col-xs-6">
                            <div class="row">
                                <div class="col-sm-3">
                                    <i class="fa fa-heartbeat"></i>
                                </div>
                                <div class="col-sm-9">
                                    <h4>Saúde</h4>
                                    <p><a href="listings.html" >Enfermeira</a>, <a href="listings.html" >Fisioterapeuta</a>, <a href="listings.html" >Nutricionista</a>, <a href="listings.html">Fonoaudiólogo</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="directory-block col-sm-4 col-xs-6">
                            <div class="row">
                                <div class="col-sm-3">
                                    <i class="fa fa-desktop"></i>
                                </div>
                                <div class="col-sm-9">
                                    <h4>Tecnologia</h4>
                                    <p><a href="listings.html" >Aúdio e Vídeo</a>, <a href="listings.html" >Animação</a>, <a href="listings.html" >Modelagem 3D</a>, <a href="listings.html">Edição de Fotos</a>,</p>
                                </div>
                            </div>
                        </div>
                        <div class="directory-block col-sm-4 col-xs-6">
                            <div class="row">
                                <div class="col-sm-3">
                                    <i class="fa fa-star"></i>
                                </div>
                                <div class="col-sm-9">
                                    <h4>Eventos</h4>
                                    <p><a href="listings.html" >DJs</a>, <a href="listings.html" >Fotografia</a>, <a href="listings.html" >Gravação de Vídeos</a>, <a href="listings.html">Produção de Eventos</a>, <a href="listings.html" >Recepcionistas</a>,</p>
                                </div>
                            </div>
                        </div>
                        <div class="directory-block col-sm-4 col-xs-6">
                            <div class="row">
                                <div class="col-sm-3">
                                    <i class="fa fa-pencil"></i>
                                </div>
                                <div class="col-sm-9">
                                    <h4>Aulas</h4>
                                    <p><a href="listings.html" >Dança</a>, <a href="listings.html" >Idiomas</a>, <a href="listings.html" >Concurso</a>, <a href="listings.html">Informática</a>, <a href="listings.html" >Música</a>, <a href="listings.html" >Esportes</a>, <a href="listings.html" >Artes</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="directory-block col-sm-4 col-xs-6">
                            <div class="row">
                                <div class="col-sm-3">
                                    <i class="fa fa-wrench"></i>
                                </div>
                                <div class="col-sm-9">
                                    <h4>Assitência Técnica</h4>
                                    <p><a href="listings.html" >Computador</a>, <a href="listings.html" >Celular</a>, <a href="listings.html" >Eletrodomésticos</a>, <a href="listings.html">Vídeo Games</a>, <a href="listings.html" >Telefonia</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row directory-counties hidden-xs">
                    <div class="col-sm-12">
                        <ul class="nav nav-tabs" id="myTab">
                            <li class=""><a data-toggle="tab"  href="#popular">Cidades</a></li>
                            <li class=""><a data-toggle="tab"  href="#EN">Recife</a></li>
                            <li class=""><a data-toggle="tab"  href="#WA">Olinda</a></li>
                            <li class=""><a data-toggle="tab"  href="#SC">Cabo</a></li>
                            <li class="hidden-md"><a data-toggle="tab"  href="#NI">Jaboatão dos Guararapes</a></li>
                            <li class="dropdown">
                                <a data-toggle="dropdown" class="dropdown-toggle" id="myTabDrop1" href="#">Em breve<b class="caret"></b></a>
                                <ul aria-labelledby="myTabDrop1" role="menu" class="dropdown-menu">
                                    <li>São Paulo</li>
                                    <li>Rio de  Janeiro</li>
                                    <li>Brasilia</li>
                                    <li>Curitiba</li>
                                    <li>Salvador</li>
                                    <li>João Pessoa</li>
                                </ul>
                            </li>
                        </ul>
                        <div class="tab-content " id="myTabContent">
                            <div id="popular" class="tab-pane fade counties-pane active">
                                <div class="col-sm-3">
                                    <div class="row directory-block">
                                        <div class="col-sm-12">
                                            <a href="listings.html">Caruaru</a><br />
                                            <a href="listings.html">Gravatá</a><br />
                                            <a href="listings.html">vitoria de Santo Antão</a><br />
                                            <a href="listings.html">Santa Cruz</a><br />
                                            <a href="listings.html">Ipojuca</a><br />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="row directory-block">
                                        <div class="col-sm-12">
                                            <a href="listings.html">Paulista</a><br />
                                            <a href="listings.html">Carpina</a><br />
                                            <a href="listings.html">Petrolina</a><br />
                                            <a href="listings.html">Garanhuns</a><br />
                                            <a href="listings.html">Abreu e Lima</a><br />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="row directory-block">
                                        <div class="col-sm-12">
                                            <a href="listings.html">Belo Jardim</a><br />
                                            <a href="listings.html">Bonito</a><br />
                                            <a href="listings.html">Catende</a><br />
                                            <a href="listings.html">Ilha de Itamaracá</a><br />
                                            <a href="listings.html">Tamandaré</a><br />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="row directory-block">
                                        <div class="col-sm-12">
                                            <a href="listings.html">Moreno</a><br />
                                            <a href="listings.html">Nazaré da Mata</a><br />
                                            <a href="listings.html">Santa Cruz do Capibaribe</a><br />
                                            <a href="listings.html">Arcoverde</a><br />
                                            <a href="listings.html">Belo Jardim</a><br />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="EN" class="tab-pane counties-pane">
                                <div class="col-sm-3">
                                    <div class="row directory-block">
                                        <div class="col-sm-12">
                                            <a href="listings.html" >Aflitos</a><br />
                                            <a href="listings.html" >Afogados</a><br />
                                            <a href="listings.html" >Água Fria</a><br />
                                            <a href="listings.html" >Alto José Bonifácio</a><br />
                                            <a href="listings.html" >Alto José do Pinho</a><br />
                                            <a href="listings.html" >Alto do Mandu</a><br />
                                            <a href="listings.html" >Alto do Pascoal</a><br />
                                            <a href="listings.html" >Alto Santa Teresinha</a><br />
                                            <a href="listings.html" >Apipucos</a><br />
                                            <a href="listings.html" >Areias</a><br />
                                            <a href="listings.html" >Arruda</a><br />
                                            <a href="listings.html" >Barro</a><br />
                                            <a href="listings.html" >Benfica</a><br />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="row directory-block">
                                        <div class="col-sm-12">                                            
                                            <a href="listings.html" >Boa Viagem</a><br />
                                            <a href="listings.html" >Boa Vista</a><br />
                                            <a href="listings.html" >Bomba do Hemetério</a><br />
                                            <a href="listings.html" >Bongi</a><br />
                                            <a href="listings.html" >Brasília Teimosa</a><br />
                                            <a href="listings.html" >Brejo do Beberibe</a><br />
                                            <a href="listings.html" >Brejo da Guabiraba</a><br />
                                            <a href="listings.html" >Cabanga</a><br />
                                            <a href="listings.html" >Caçote</a><br />
                                            <a href="listings.html" >Cajueiro</a><br />
                                            <a href="listings.html" >Campina do Barreto</a><br />
                                            <a href="listings.html" >Campo Grande</a><br />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="row directory-block">
                                        <div class="col-sm-12">
                                            <a href="listings.html" >Casa Amarela</a><br />
                                            <a href="listings.html" >Casa Forte</a><br />
                                            <a href="listings.html" >Caxangá</a><br />
                                            <a href="listings.html" >Cidade Universitária</a><br />
                                            <a href="listings.html" >Coelhos</a><br />
                                            <a href="listings.html" >Cohab</a><br />
                                            <a href="listings.html" >Comunidade do Pilar</a><br />
                                            <a href="listings.html" >Coque</a><br />
                                            <a href="listings.html" >Coqueiral</a><br />
                                            <a href="listings.html" >Cordeiro</a><br />
                                            <a href="listings.html" >Córrego do Jenipapo</a><br />
                                            <a href="listings.html" >Curado</a><br />
                                            <a href="listings.html" >Derby</a><br />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="row directory-block">
                                        <div class="col-sm-12">
                                            <a href="listings.html" >Dois Irmãos</a><br />
                                            <a href="listings.html" >Dois Unidos</a><br />
                                            <a href="listings.html" >Encruzilhada</a><br />
                                            <a href="listings.html" >Engenho do Meio</a><br />
                                            <a href="listings.html" >Entra Apulso</a><br />
                                            <a href="listings.html" >Espinheiro</a><br />
                                            <a href="listings.html" >Estância</a><br />
                                            <a href="listings.html" >Fundão</a><br />
                                            <a href="listings.html" >Graças</a><br />
                                            <a href="listings.html" >Guabiraba</a><br />
                                            <a href="listings.html" >Hipódromo</a><br />
                                            <a href="listings.html" >Ibura</a><br />
                                            <a href="listings.html" >Ilha Joana Bezerra</a><br />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="WA" class="tab-pane counties-pane">
                                <div class="col-sm-3">
                                    <div class="row directory-block">
                                        <div class="col-sm-12">
                                            <a href="listings.html" >Águas Compridas</a><br />
                                            <a href="listings.html" >Aguazinha</a><br />
                                            <a href="listings.html" >Alto da Bondade</a><br />
                                            <a href="listings.html" >Alto da Conquista</a><br />
                                            <a href="listings.html" >Alto da Nação</a><br />
                                            <a href="listings.html" >Alto do Sol Nascente</a><br />
                                            <a href="listings.html" >Amaro Branco</a><br />
                                            <a href="listings.html" >Amparo</a><br />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="row directory-block">
                                        <div class="col-sm-12">
                                            <a href="listings.html" >Bairro Novo</a><br />
                                            <a href="listings.html" >Bonsucesso</a><br />
                                            <a href="listings.html" >Bultrins</a><br />
                                            <a href="listings.html" >Casa Caiada</a><br />
                                            <a href="listings.html" >Caixa D'água</a><br />
                                            <a href="listings.html" >Carmo</a><br />
                                            <a href="listings.html" >Fragoso</a><br />
                                            <a href="listings.html" >Guadalupe</a><br />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="row directory-block">
                                        <div class="col-sm-12">
                                            <a href="listings.html" >Jardim Atlântico</a><br />
                                            <a href="listings.html" >Jardim Brasil</a><br />
                                            <a href="listings.html" >Milagres</a><br />
                                            <a href="listings.html" >Monte</a><br />
                                            <a href="listings.html" >Ouro Preto</a><br />
                                            <a href="listings.html" >Passarinho</a><br />
                                            <a href="listings.html" >Peixinhos</a><br />
                                            <a href="listings.html" >Rio Doce</a><br />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="row directory-block">
                                        <div class="col-sm-12">
                                            <a href="listings.html" >Santa Tereza</a><br />
                                            <a href="listings.html" >Salgadinho</a><br />
                                            <a href="listings.html" >São Benedito</a><br />
                                            <a href="listings.html" >Sapucaia</a><br />
                                            <a href="listings.html" >Sítio Novo</a><br />
                                            <a href="listings.html" >Tabajara</a><br />
                                            <a href="listings.html" >Varadouro</a><br />
                                            <a href="listings.html" >Vila Popular</a><br />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="SC" class="tab-pane counties-pane">
                                <div class="col-sm-3">
                                    <div class="row directory-block">
                                        <div class="col-sm-12">
                                            <a href="listings.html" >Alto da Bela Vista</a><br />
                                            <a href="listings.html" >Bela Vista</a><br />
                                            <a href="listings.html" >Bom Conselho</a><br />
                                            <a href="listings.html" >Charneca</a><br />
                                            <a href="listings.html" >Charnequinha</a><br />
                                            <a href="listings.html" >Destilaria</a><br />
                                            <a href="listings.html" >Distrito Industrial</a><br />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="row directory-block">
                                        <div class="col-sm-12">                                            
                                            <a href="listings.html" >Distrito Industrial Santo Estevão</a><br />
                                            <a href="listings.html" >Engenho Ilha</a><br />
                                            <a href="listings.html" >Enseada dos Corais</a><br />
                                            <a href="listings.html" >Gaibu</a><br />
                                            <a href="listings.html" >Garapu</a><br />
                                            <a href="listings.html" >Itapuama</a><br />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="row directory-block">
                                        <div class="col-sm-12">                                            
                                            <a href="listings.html" >Malaquias</a><br />
                                            <a href="listings.html" >Paiva</a><br />
                                            <a href="listings.html" >Pirapama</a><br />
                                            <a href="listings.html" >Ponte dos Carvalhos</a><br />
                                            <a href="listings.html" >Pontezinha</a><br />
                                            <a href="listings.html" >Jardim Santo Inácio</a><br />
                                            <a href="listings.html" >Suape</a><br />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="row directory-block">
                                        <div class="col-sm-12">
                                            <a href="listings.html" >Vila Social Contra Mocambo</a><br />
                                            <a href="listings.html" >Centro</a><br />
                                            <a href="listings.html" >Cohab</a><br />
                                            <a href="listings.html" >Rosário</a><br />
                                            <a href="listings.html" >São Francisco</a><br />
                                            <a href="listings.html" >Mercês</a><br />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="NI" class="tab-pane counties-pane">
                                <div class="col-sm-3">
                                    <div class="row directory-block">
                                        <div class="col-sm-12">
                                            <a href="listings.html" >Barra de Jangada</a><br />
                                            <a href="listings.html" >Bulhões</a><br />
                                            <a href="listings.html" >Cajueiro Seco</a><br />
                                            <a href="listings.html" >Candeias</a><br />
                                            <a href="listings.html" >Cavaleiro</a><br />
                                            <a href="listings.html" >Conjunto Muribeca</a><br />                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="row directory-block">
                                        <div class="col-sm-12">                                                                                        
                                            <a href="listings.html" >Curado</a><br />
                                            <a href="listings.html" >Engenho Velho</a><br />
                                            <a href="listings.html" >Floriano</a><br />
                                            <a href="listings.html" >Jaboatão</a><br />
                                            <a href="listings.html" >Manaçu</a><br />
                                            <a href="listings.html" >Muribeca dos Guararapes</a><br />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="row directory-block">
                                        <div class="col-sm-12">                                                                                        
                                            <a href="listings.html" >Muribequinha</a><br />
                                            <a href="listings.html" >Piedade</a><br />
                                            <a href="listings.html" >Prazeres</a><br />
                                            <a href="listings.html" >Santana</a><br />
                                            <a href="listings.html" >Santo Aleixo</a><br />
                                            <a href="listings.html" >Socorro</a><br />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="row directory-block">
                                        <div class="col-sm-12">                                                                                        
                                            <a href="listings.html" >Vargem Fria</a><br />
                                            <a href="listings.html" >Vila Rica</a><br />
                                            <a href="listings.html" >Vista Alegre</a><br />
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-4 " >
                <div class="row">
                    <div class="col-xs-12 col-sm-3 col-md-12  col-lg-11 pull-right" >
                        <br class="hidden-sm hidden-xs"/>
                        <br class="hidden-sm hidden-xs"/>
                        <br class="hidden-sm hidden-xs"/>
                        <div class="panel panel-default">
                            <div class="panel-heading">Guia rápido</div>
                            <ul class="list-group">
                                <li class="list-group-item"><a href="typography.html">Nossas dicas para ficar seguro</a></li>
                                <li class="list-group-item"><a href="typography.html">Como comprar plano</a></li>
                                <li class="list-group-item"><a href="typography.html">Como oferecer serviços</a></li>
                                <li class="list-group-item"><a href="typography.html">Ajuda e entre em contato conosco</a></li>
                                <li class="list-group-item"><a href="typography.html">Perguntas frequentes</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-5 col-md-12  col-lg-11 pull-right" >
                        <div class="panel panel-default">
                            <div class="panel-body" style="height: 102px; display: block;">
                                <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-width="265" data-layout="standard" data-action="like" data-show-faces="false" data-share="false" style="display: block; height: 30px;"></div>
                                <br />
                                <!-- Place this tag where you want the +1 button to render. -->
                                <div class="g-plusone" data-annotation="inline" data-width="300" style="display: block; height: 30px;"></div>
                                <!-- Place this tag after the last +1 button tag. -->
                                <script type="text/javascript">
                                (function() {
                                    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                                    po.src = '../../apis.google.com/js/platform.js';
                                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
                                })();
                                </script>
                            </div>
                            <div class="panel-footer">
                                <a href="https://twitter.com/twitterapi" class="twitter-follow-button" data-dnt="true">Propaganda</a>
                                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="../../platform.twitter.com/widgets.html";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                            </div>
                        </div>
                        <p class="main_slogan" style="margin: 28px 0">Propaganda</p>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-12  col-lg-11 pull-right" >
                        <div class="panel panel-default">
                            <div class="panel-heading">Anúncios Premium</div>
                            <div class="panel-body">
                                <div class="featured-gallery">
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-4 featured-thumbnail"  data-toggle="tooltip" data-placement="top" title="Programmer job availiable at Uber in London">
                                            <a href="details.html" class="">
                                                <img alt="" src="css/images/logos/uberlogo_large_verge_medium_landscape.png" style="width: 100%" >
                                            </a>
                                        </div>
                                        <div class="col-sm-6 col-xs-4 featured-thumbnail"  data-toggle="tooltip" data-placement="top"  title="Porsche Boxster S, 2.9 2dr reg Apr 2007 ">
                                            <a href="details.html" class="">
                                                <img alt="" src="css/images/logos/car-78738_150.jpg"  />
                                            </a>
                                        </div>
                                        <div class="col-sm-6 col-xs-4 featured-thumbnail" data-toggle="tooltip" data-placement="top"  title="Please find my lost cat">
                                            <a href="details.html" class="" >
                                                <img alt="" src="css/images/logos/cats-q-c-120-80-4.jpg"  />
                                            </a>
                                        </div>
                                        <div class="col-sm-6 col-xs-4 featured-thumbnail" data-toggle="tooltip" data-placement="top"  title="Mini copper looking for a quick sell !! - London - £2,485">
                                            <a href="details.html" class="" >
                                                <img alt="" src="css/images/logos/transport-q-c-120-80-8.jpg"  />
                                            </a>
                                        </div>
                                        <div class="col-sm-6 col-xs-4 featured-thumbnail" data-toggle="tooltip" data-placement="top"  title="Old MP3 player for sale">
                                            <a href="details.html" class="" >
                                                <img alt="" src="css/images/logos/technics-q-c-120-80-10.jpg"  />
                                            </a>
                                        </div>
                                        <div class="col-sm-6 col-xs-4 featured-thumbnail" data-toggle="tooltip" data-placement="top" title="Designer job availiable at Uber in London">
                                            <a href="details.html" class="" >
                                                <img alt="" src="css/images/logos/uberlogo_large_verge_medium_landscape.png"  />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /.container --><!-- Modal -->


        <script src="js/categoria.js"></script>
        
        <?php include 'footer.php'; ?>

    </body>
    <!-- Mirrored from templates.expresspixel.com/bootlistings/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 25 Feb 2016 10:59:56 GMT -->
    </html>