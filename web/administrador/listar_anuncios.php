<?php 
    session_start("backoffice");

    include 'head.php'; 
    include 'util/util.php'; 

    if($ordem == ""){
        $ordem = "servico.id";
    }

    if($acao == "pesquisar"){
        unset($nome);
    }

    header('Access-Control-Allow-Origin: *'); 
?>
<body class="cl-default fixed">
    <?php include 'nav_bar_top.php'; ?>


    <!-- start:wrapper body -->
    <div class="wrapper row-offcanvas row-offcanvas-left">

        <!-- end:left sidebar -->
        <?php include 'nav_menu_left.php'; ?>
        <!-- start:right sidebar -->

        <aside class="right-side">
            <section class="content">
                <h1>
                    Anuncios
                    <small>Listagem de Anuncios Pendentes</small>
                </h1>
                <!-- start:breadcrumb -->
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-image"></i> Anuncios</a></li>
                    <li><a href="listar_pendentes.php"> Anuncios Pendentes</a></li>
                    <!--<li class="active">Blank Page</li> -->
                </ol>
                <!-- end:breadcrumb -->
                <?php 
                    $filtros = "";
                    $request = array();
                    if($nome != ""){
                        $request['nome'] = $nome;
                    }

                    $ch = curl_init($_SESSION['caminhoWS'].'ServicoModel/consultarRows');
                    curl_setopt($ch, CURLOPT_POST, true);                                                                    
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $request);                                                                  
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                 
                    
                    $qtd = json_decode(curl_exec($ch), true);
                    include("util/paginacao.php");

                    curl_close($ch);

                    if($ordem != ""){
                         $filtros .= " ORDER BY " . str_replace("_desc", " DESC", $ordem);
                    }
                    $filtros .= " LIMIT $inicio, ". $_SESSION['qtdList'];
                    $request['filtros'] = $filtros;

                    $ch = curl_init($_SESSION['caminhoWS'].'ServicoModel/listServicosPendentes');

                    curl_setopt($ch, CURLOPT_POST, true);                                                                    
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $request);                                                                  
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                 
                    
                    $arrAnuncios = json_decode(curl_exec($ch), true);

                    curl_close($ch);
                ?>
                <!-- start:content -->
                <form name="form1" id="form1" method="post">
                    <input type="hidden" name="ordem" id="ordem" value="<?php print($ordem) ?>">
                    <input type="hidden" name="pag" id="pag" value="<?php print($pag) ?>">
                    <input type="hidden" name="acao" id="acao">
                    <input type="hidden" name="msgok" id="msgok">
                    <input type="hidden" name="aprovado" id="aprovado">
                    <input type="hidden" name="codusuario" id="codusuario">
                    <div class="row">
                        <div class="col-lg-12">
                            <?php include 'util/box_messages.php'; ?>
                            <div class="panel">
                                <div class="panel-body">
                                    <div class="form-inline" role="form">
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="nome" class="control-label">Nome:</label>
                                            </div>
                                            <div class="form-group">
                                                <input  type="text" style="width: 230px" class="form-control input-sm" name="nome" id="nome" value="<?php print($nome) ?>">
                                            </div>
                                        </div>
                                        <div class="col-lg-3 pull-right">
                                            <div class="form-group pull-right">
                                                <?php if($nome != ""){ ?>
                                                <button type="button" onClick="Pesquisar('<?php print($ordem) ?>', '', 'S')" class="btn btn-warning"><i class="fa fa-ban"></i> Cancelar</button>
                                            <?php }else{ ?>
                                                <button type="button" onClick="Pesquisar('<?php print($ordem) ?>', '', 'N')" class="btn btn-success"><i class="fa fa-ok"></i>Filtrar</button>
                                            <?php } ?>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-success" data-original-title="" title=""><i class="fa fa-cog"></i> Ações</button>
                                                <button type="button" class="btn btn-success active dropdown-toggle" data-toggle="dropdown" data-original-title="" title="">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="manter_plano.php">Incluir</a></li>
                                                </ul>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box blank-page">
                                <?php include "util/links_paginacao.php"; ?>
                                <div class="col-md-12">
                                    <table class="table table-striped table-advance table-hover">
                                    <thead>
                                        <tr>
                                            <th style="cursor: pointer;" onClick="Pesquisar('servico.id<?php if($ordem == "servico.id"){ ?>_desc<?php } ?>', '');" <?php if($ordem == "servico.id"){ ?>class="fa fa-sort-asc"<?php } else if($ordem == "servico.id_desc"){ ?>class="fa fa-sort-desc"<?php } ?>> ID </th>
                                            <th style="cursor: pointer;" onClick="Pesquisar('usuario.nome<?php if($ordem == "usuario.nome"){ ?>_desc<?php } ?>', '');" <?php if($ordem == "usuario.nome"){ ?>class="fa fa-sort-asc"<?php } else if($ordem == "usuario.nome_desc"){ ?>class="fa fa-sort-desc"<?php } ?>> Usuario </th>
                                            <th style="cursor: pointer;" onClick="Pesquisar('servico.titulo<?php if($ordem == "servico.titulo"){ ?>_desc<?php } ?>', '');" <?php if($ordem == "servico.titulo"){ ?>class="fa fa-sort-asc"<?php } else if($ordem == "servico.titulo_desc"){ ?>class="fa fa-sort-desc"<?php } ?>> Titulo </th>
                                            <th style="cursor: pointer;" onClick="Pesquisar('servico.valor<?php if($ordem == "servico.valor"){ ?>_desc<?php } ?>', '');" <?php if($ordem == "servico.valor"){ ?>class="fa fa-sort-asc"<?php } else if($ordem == "servico.valor"){ ?>class="fa fa-sort-desc"<?php } ?>> Valor </th>
                                             <th style="cursor: pointer;" onClick="Pesquisar('servico.data_cadastro<?php if($ordem == "servico.data_cadastro"){ ?>_desc<?php } ?>', '');" <?php if($ordem == "servico.data_cadastro"){ ?>class="fa fa-sort-asc"<?php } else if($ordem == "servico.data_cadastro"){ ?>class="fa fa-sort-desc"<?php } ?>> Data </th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($arrAnuncios as $anuncios => $anuncio) { ?>
                                        <tr>
                                            <td> <?php echo $anuncio['id']; ?> </td>
                                            <td> <?php echo $anuncio['nomeusuario']; ?> </td>
                                            <td> <?php echo $anuncio['titulo']; ?> </td>
                                            <td> R$ <?php echo FormatarValorExibir($anuncio['valor']); ?> </td>
                                            <td> <?php echo FormatarDataHoraExibir(str_replace(" ","",str_replace(":","",str_replace("-","",$anuncio['data_cadastro'])))); ?> </td>
                                            <td>
                                                <a class="btn btn-primary btn-xs"; title="Validar" onClick="Validar(<?php echo $anuncio['id']; ?>)">
                                                <i class="fa fa-bug" title="Validar"></i>  <strong>Validar</strong> </a>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                </div>
                                <?php include "util/links_paginacao.php"; ?>
                            </div>
                        </div>
                       <div class="modal fade" id="modalAprovado" tabindex="-1" role="dialog">
                          <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title">Validação de Anuncio</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>Anuncio validado com sucesso! Deseja Aprova - lo agora?</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Não</button>
                                        <button type="button" id="btnAprovado" class="btn btn-primary">Sim</button>
                                    </div>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->
                        <div class="modal fade" id="modalReprovado" tabindex="-1" role="dialog">
                          <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title">Validação de Anuncio</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>Anuncio reprovado. Será enviado um E-Mail para o Usuario revisa - lo!</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Ok</button>
                                    </div>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->
                    </div>
                </form>
                <!-- end:content -->

            </section>
        </aside>
        <!-- end:right sidebar -->

    </div>
    <!-- end:wrapper body -->

</body>
<script type="text/javascript">

$("#btnAprovado").on("click",function(){
    var codservico =  $("#aprovado").val(); 
    var enviarEmail = {};
    enviarEmail.codusuario = $("#codusuario").val();
    enviarEmail.msgEmail = "ap";
    var codusuario =   
    $.ajax({
        url:"http://portservise.esy.es/portservise_webservice/ServicoModel/aprovarServico",
        type:"POST",
        dataType:"JSON",
        data:{codservico:codservico},
        success:function(data){
            console.log(data);
            $.ajax({
                url: "util/enviar_emails.php",
                type: "GET",
                data:enviarEmail,
                success:function(result){
                    console.log(result);
                },
                error:function(result){
                    console.log(result);
                }
            });

            $("#msgok").val("Anuncio Aprovado com sucesso!");
            $("#form1").submit();
        },
        error:function(data){
            console.log(data);  
        }
    });
});

function Pesquisar(ordem, pag, ativa){
    $("#ordem").val(ordem);
    $("#pag").val(pag);

    if(ativa == "S"){
        $("#acao").val("pesquisar");
    }
    $("#form1").submit();
}

function Validar(cod){
    $("#aprovado").val("");
    $("#codusuario").val("");
    var request = {};
    var enviarEmail = {};
    request.codservico = cod;
    $.ajax({
        type:"GET",
        url:"util/validarAnuncio.php?codservico="+cod,
        success:function(data){
            console.log(data);
            var dados = jQuery.parseJSON( data );
            enviarEmail.codusuario = dados.codusuario;
            enviarEmail.msgEmail = "rp";
            if(dados.aprovado){
                $("#aprovado").val(cod);
                $("#codusuario").val(dados.codusuario);
                $("#modalAprovado").modal("show");
            }else{
                $.ajax({
                    url: "util/enviar_emails.php",
                    type: "GET",
                    data:enviarEmail,
                    success:function(result){
                        console.log(result);
                    },
                    error:function(result){
                        console.log(result);
                    }
                });
                $("#modalReprovado").modal("show");
            }
        },
        error:function(data){
            console.log(data); 
        }
    });
}

</script>

<!-- Mirrored from bootemplates.com/themes/arjuna/blank.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 23 Oct 2015 22:39:08 GMT -->
</html>