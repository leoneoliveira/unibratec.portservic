<?php 
    session_start("backoffice");

    include 'head.php'; 
    include 'util/util.php'; 


    if($acao == "salvar" || $acao == "salvarcontinuar"){

        $request = array();

        if($nome != ""){
            $request['nome'] =  $nome;
        }

        if($descricao != ""){
            $request['descricao'] =  $descricao;
        }

        if($preco != ""){
            $request['preco'] = FormatarValorBanco($preco);
        }

        if($status != ""){
            $request['status'] =  $status;
        }else{
             $request['status'] =  'I';
        }

        if($codplano != ""){
            $request['codplano'] =  $codplano;
        }

        $ch = curl_init($_SESSION['caminhoWS'].'PlanoModel/manterPlano');

        curl_setopt($ch, CURLOPT_POST, true);                                                                    
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        
        $msg = json_decode(curl_exec($ch), true);
        curl_close($ch);

        if($msg['tipo'] == "update"){
            $msgok = "Manutenção realizada com sucesso!";
        }else{
            $msgok = "Criação de registro realizada com sucesso!";
        }
        echo var_dump($request);
        if($acao == 'salvar'){
            header("Location: listar_planos.php?msgok=$msgok");
            exit;
        }
    }

    if($codplano != ""){
        $request = array('codplano' => $codplano);
        
        $ch = curl_init($_SESSION['caminhoWS'].'PlanoModel/listPlanosById');

        curl_setopt($ch, CURLOPT_POST, true);                                                                    
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        
        $arrPlanos = json_decode(curl_exec($ch), true);

        curl_close($ch);

        foreach ($arrPlanos as $planos => $plano) {
            $codplano = $plano['plano_id'];
            $nome = $plano['nome'];
            $descricao = $plano['descricao'];
            $preco = FormatarValorExibir($plano['preco']);
            $status = $plano['status'];
        }
    }
      
?>
<body class="cl-default fixed">
<?php 
    include 'nav_bar_top.php'; ?>

    <!-- start:wrapper body -->
    <div class="wrapper row-offcanvas row-offcanvas-left">

        <!-- end:left sidebar -->
        <?php include 'nav_menu_left.php'; ?>
        <!-- start:right sidebar -->

        <aside class="right-side">
            <section class="content">
                <h1>
                    Plano
                    <small>Alterar Plano</small>
                </h1>
                <!-- start:breadcrumb -->
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-cog"></i> Cadastros</a></li>
                    <li><a href="listar_categorias.php"> Planos</a></li>
                    <li><a href="#"> Alterar Plano</a></li>
                    <!--<li class="active">Blank Page</li> -->
                </ol>
                <!-- end:breadcrumb -->
                <!-- start:content -->
                <?php include 'util/box_messages.php'; ?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel">
                            <div class="panel-body">
                                <div class="form-inline">
                                    <div class="col-lg-4 pull-right">
                                        <button type="button" style="margin-left: 12px;" onClick="Salvar('salvar')" class="btn btn-success pull-right " title="">Salvar</button>
                                        <button type="button" class="btn btn-success pull-right"onClick="Salvar('salvarcontinuar')" title="">Salvar & Continuar</button>
                                        <button type="button" style="margin-right: 8px;" onClick="window.location.href='listar_planos.php'" class="btn btn-default pull-right " title="">Voltar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                       <div class="box blank-page">
                            <div class="col-md-12">
                                <div class="col-lg-4 pull-right">
                                    
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="panel-body">
                                    <form name="form1" id="form1" class="form-horizontal tasi-form" method="post">
                                        <!-- categoria_id-->
                                        <input type="hidden" name="acao" id="acao">
                                        <input type="hidden" name="codplano" id="codplano" value="<?php echo $codplano; ?>">
                                        <div class="form-group">
                                            <label class="col-sm-2 col-sm-2 control-label">Plano Ativa?</label>
                                            <div class="col-sm-10">
                                                <input type="checkbox" name="status" <?php if($status == "A" || $status == ""){ ?>checked <?php } ?> value="A" data-on-text="Ativo" data-off-text="Inativo" data-on-color="success" data-off-color="warning">
                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <label class="col-sm-2 col-sm-2 control-label">Nome</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="nome" value="<?php echo $nome; ?>" id="nome" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 col-sm-2 control-label">Pre&ccedil;o</label>
                                            <div class="col-sm-10">
                                                <input type="text" id="preco" name="preco" value="<?php echo $preco; ?>" id="preco" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 col-sm-2 control-label">Descri&ccedil;&atilde;o</label>
                                            <div class="col-sm-10">
                                                 <textarea class="form-control ckeditor" name="descricao" rows="6"><?php echo $descricao; ?></textarea>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
                <!-- end:content -->

            </section>
        </aside>
        <!-- end:right sidebar -->

    </div>
    <!-- end:wrapper body -->

</body>
<script src="js/form.component.js"></script>
<script src="js/jquery.validate.min.js" type="text/javascript"></script>
<script type="text/javascript" src="plugins/jquery-price-format/jquery.price_format.js"></script>
<!--<script src="plugins/ckeditor/ckeditor.js"></script> -->
<script type="text/javascript">
$("[name='status']").bootstrapSwitch();

function Salvar(tipo){
    $("#acao").val(tipo);
    $("#form1").submit();
}


$(document).ready(function () {

     $("#form1").validate({
        focusInvalid: true, 
        rules: {
            nome: {
                required: true
            }
        },

        invalidHandler: function (event, validator) {              
            $('#alert-danger').text('Os campos marcados são de preenchimento obrigatório');
            $('#alert-danger').show();
            $('#alert-warning').hide();
            $('#alert-success').hide();
        },

        errorPlacement: function (error, element) { 
            
        },

        highlight: function (element) { 
            $(element)
            .closest('.form-group').addClass('has-error'); 
        },

        unhighlight: function (element) {
            
        },

        success: function (label, element) {
            $(element).closest('.form-group').removeClass('has-error'); 
        },

        submitHandler: function (form) {
            form.submit();
        }
    });

    $('#preco').priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.'
    }); 
});
</script>
<!-- Mirrored from bootemplates.com/themes/arjuna/blank.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 23 Oct 2015 22:39:08 GMT -->
</html>