    <!-- start:navbar top -->
    <header class="header">
        <a href="dashboard.php" class="logo">Port Service
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="navbar-btn sidebar-toggle" data-target="#atas" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <div class="collapse navbar-collapse" id="atas">
                
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="img/user.jpg" class="img-circle img-responsive img-user">
                                <strong><?php echo $_SESSION['usuario']['nome']; ?></strong>
                            </a>
                            <ul class="dropdown-menu dropdown-login">
                                <li>
                                    <div class="navbar-login">
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <p class="text-center">
                                                    <img src="img/user.jpg" class="img-responsive img-circle">
                                                </p>
                                            </div>
                                            <div class="col-lg-9">
                                                <p class="text-left"><strong><?php echo $_SESSION['usuario']['nome']; ?></strong></p>
                                                <p class="text-left small"><?php echo $_SESSION['usuario']['email']; ?></p>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <small><a href="#">Meus dados</a></small>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <small><a href="#">Alterar senha</a></small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <div class="navbar-login navbar-login-session">
                                        <a href="index.php?acao=logout&msgok=Volte sempre!" class="btn btn-default btn-square btn-block">Logout</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <!-- end:navbar top -->