<!DOCTYPE html>
<html lang="en">
<?php 
    session_start("backoffice"); 
    
    include 'head.php';
    include 'util/util.php'; 

    $_SESSION['caminhoWS'] = "http://portservise.esy.es/portservise_webservice/";
    
    if($acao == 'logout'){
        session_destroy();
        setcookie('usuario_id');
    }
    elseif($acao == 'login'){

        $request = array( 'email' => $email, 'senha' => $password );

        $ch = curl_init($_SESSION['caminhoWS'].'UsuarioModel/logarAdmin');

        curl_setopt($ch, CURLOPT_POST, true);                                                                    
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        
        $result = json_decode(curl_exec($ch), true);
        curl_close($ch);

        $_SESSION['usuario'] = $result['dados'];
        if(count($_SESSION['usuario']) > 0){
            if($lembrar == 'S'){
                setcookie('usuario_id', $_SESSION['usuario']['id'] , time()+(60*60*24*365));
            }

            header("Location: dashboard.php");
            exit;
        }
        else{
            $msgwar = "E-mail ou senha inválido!";
        }
    }
    elseif(isset($_COOKIE['usuario_id'])){
        $request = array( 'id' => $_COOKIE['usuario_id'] );

        $ch = curl_init($_SESSION['caminhoWS'].'UsuarioModel/listUsuariosById');

        curl_setopt($ch, CURLOPT_POST, true);                                                                    
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        
        $result = json_decode(curl_exec($ch), true);
        curl_close($ch);

        $_SESSION['usuario'] = $result['dados'];
        if(count($_SESSION['usuario']) > 0){

            header("Location: dashboard.php");
            exit;
        }
    }

?>
<body class="cl-default fixed" id="login">

    <!-- start:wrapper -->
    <div class="header-login">
        <div class="text-center">
            <h1><i class="fa fa-pied-piper-alt"></i> PostService Admin Panel </h1>
        </div>
    </div>
 
    <div class="body-login login">
       <?php include 'util/box_messages.php'; ?>
        <form role="form" id="form1" name="form1" method="post">
            <input type="hidden" name="acao" value="login">
            <div class="form-group">
                <input type="email" name="email" id="email" class="form-control input-lg" id="email" placeholder="Enter email">
            </div>
            <div class="form-group">
                <input type="password" name="password" id="password" class="form-control input-lg" id="password" placeholder="Enter Password">
            </div>
            <div class="checkbox">
                <label>
                <input type="checkbox" name="lembrar" value="S"> Remember me
                </label>
            </div>
            <button type="submit" id="btnLogin" class="btn btn-default btn-block btn-lg btn-perspective">LOGIN</button>
        </form>
        <div class="text-center">
            <p><a href="forgot.php" ><strong>Esqueci a senha</strong></a></p>
        </div>
    </div>
</body>
<script src="js/jquery.validate.min.js" type="text/javascript"></script>
<script>

$(document).ready(function () {

     $("#form1").validate({
        focusInvalid: true, 
        rules: {
            email: {
                email: true,
                required: true
            },
            password: {
                required: true,
            }
        },

        invalidHandler: function (event, validator) {              
            $('#alert-danger').text('Os campos marcados são de preenchimento obrigatório');
            $('#alert-danger').show();
            $('#alert-warning').hide();
            $('#alert-success').hide();
        },

        errorPlacement: function (error, element) { 
            
        },

        highlight: function (element) { 
            $(element)
            .closest('.form-group').addClass('has-error'); 
        },

        unhighlight: function (element) {
            
        },

        success: function (label, element) {
            $(element).closest('.form-group').removeClass('has-error'); 
        },

        submitHandler: function (form) {
            form.submit();
        }
    });
});
</script>
    
<!-- Mirrored from bootemplates.com/themes/arjuna/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 23 Oct 2015 22:39:08 GMT -->
</html>