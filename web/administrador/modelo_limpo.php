<?php include 'head.php'; ?>
<body class="cl-default fixed">
    <?php include 'nav_bar_top.php'; ?>


    <!-- start:wrapper body -->
    <div class="wrapper row-offcanvas row-offcanvas-left">

        <!-- end:left sidebar -->
        <?php include 'nav_menu_left.php'; ?>
        <!-- start:right sidebar -->

        <aside class="right-side">
            <section class="content">
                <h1>
                    Blank Pages
                    <small>Control panel</small>
                </h1>
                <!-- start:breadcrumb -->
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-home"></i> Dashboard</a></li>
                    <li><a href="#"> Extras Pages</a></li>
                    <li class="active">Blank Page</li>
                </ol>
                <!-- end:breadcrumb -->

                <!-- start:content -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="box blank-page">
                            Goes Page Here.
                        </div>
                    </div>
                </div>
                <!-- end:content -->

            </section>
        </aside>
        <!-- end:right sidebar -->

    </div>
    <!-- end:wrapper body -->

</body>

<!-- Mirrored from bootemplates.com/themes/arjuna/blank.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 23 Oct 2015 22:39:08 GMT -->
</html>