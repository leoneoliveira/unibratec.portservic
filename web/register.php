<?php include 'head.php'; ?>
<body>
    <?php include 'nav_top.php';?>
    <hr class="topbar"/>
    <div class="container">
        <br />
        <div class="row">
            <div class="col-sm-12">
                <h1>Crie uma conta</h1>
                <hr />
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <form class="form-vertical">
                            <fieldset>
                                
                                <div class="alert alert-danger" id="danger" style="display:none;">
                                    <strong>Alerta!</strong>
                                    <span id="span_alerta"></span>
                                </div>
                                
                                <div class="row">
                                    <div class="col-sm-12" >
                                        <form class="form-vertical" id="form_cadastro_usuario">
                                            <div class="well">
                                                <div class="form-group">
                                                    <label for="nome">Nome completo</label>
                                                    <input type="email" class="form-control " id="nome" name="nome" placeholder="Informe seu nome completo">
                                                    <!--<div class="row">
                                                        <div class="col-sm-10">
                                                            <input type="email" class="form-control " id="exampleInputEmail1" placeholder="Primeiro nome">
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <input type="email" class="form-control " id="exampleInputEmail1" placeholder="Último nome">
                                                        </div>
                                                    </div>-->
                                                </div>
                                                <div class="form-group">
                                                    <label for="email">Endereço de e-mail</label>
                                                    <input type="email" class="form-control " id="email" name="email" placeholder="Informe seu email">
                                                </div>
                                                <div class="form-group">
                                                    <label for="senha">Senha</label>
                                                    <input type="password" class="form-control" id="senha" name="senha" placeholder="Certifique-se de sua senha é maior que 6 caracteres">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Confirme a Senha</label>
                                                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Confirme sua senha">
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox"> Podemos contactá-lo com propriedades relevantes, ofertas e novidades
                                                    </label>
                                                </div>
                                                <br />
                                                <a ref="account_dashboard.php" id="cadastro_usuario" class="btn btn-primary">Criar Conta</a>
                                            </div>
                                        </div>
                                    </form>
                                    
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 account-sidebar hidden-sm">
                                <div class="row">
                                    <div class="col-sm-3" style="text-align: center;">
                                        <img src="css/images/icons/Crest.png" width="50"/>
                                    </div>
                                    <div class="col-sm-8">
                                        <h3>Por que nós?</h3>
                                        <p>Nós somos uma das empresas mais conhecidas, atraindo milhares de prestadores de serviço a cada mês.<p>
                                        </div>
                                    </div>
                                    <br />
                                    <div class="row">
                                        <div class="col-sm-3" style="text-align: center;">
                                            <img src="css/images/icons/Pie-Chart.png" width="40"/>
                                        </div>
                                        <div class="col-sm-8">
                                            <h3>Ímã para os compradores</h3>
                                            <p>Temos certeza de suas listas receber a exposição máxima e é apresentado de uma forma atrativa</p>
                                        </div>
                                    </div>
                                    <br />
                                    <div class="row">
                                        <div class="col-sm-3" style="text-align: center;">
                                            <img src="css/images/icons/Search.png" width="40"/>
                                        </div>
                                        <div class="col-sm-8">
                                            <h3>Pesquisas focalizadas</h3>
                                            <p>Nossa tecnologia e algoritmo corresponde potenciais servidores diretamente para a sua lista</p>
                                        </div>
                                    </div>
                                    <br />
                                    <div class="row">
                                        <div class="col-sm-3" style="text-align: center;">
                                            <img src="css/images/icons/Telephone.png" width="40"/>
                                        </div>
                                        <div class="col-sm-8">
                                            <h3>Web Móvel</h3>
                                            <p>Suas listagens será sempre acessível a todos, mesmo quando estão em movimento, através do nosso site para celular responsiva</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div><!-- Modal -->
    
    <script src="js/jquery.js"></script>
    <script src="js/usuario.js"></script>
    <?php include 'footer.php'; ?>
</body>
<!-- Mirrored from templates.expresspixel.com/bootlistings/register.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 25 Feb 2016 10:59:57 GMT -->
</html>