<?php
date_default_timezone_set("America/Recife");
error_reporting(0);

/////////////////////////////////////////////////
//Configurações do e-mail
/////////////////////////////////////////////////
define('EMAIL_SMTP', 'smtp.gmail.com');
define('EMAIL_NAME', 'PortService');
define('EMAIL_ACCOUNT', 'netinhohs90@gmail.com');
define('EMAIL_PASSWORD', 'senhaunica');
define('EMAIL_AUT', true);
define('EMAIL_SEC', '');
define('EMAIL_PORT', '587');

$_SESSION['qtdList'] = 12;


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funcção para evitar chamda $_GET, $_POST exemplo: $_GET['codusuario'] =é a mesma coisa que: $codusuario
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function register_globals_on(){
  if($_POST){
      foreach($_POST as $var=>$value){
          global $$var;
          $$var = $value;
      }
  }
  else if($_GET){
      foreach($_GET as $var=>$value){
          global $$var;
          $$var = $value;

      }
  }
  else if($_SESSION){
      foreach($_SESSION as $var=>$value){
          global $$var;
          $$var = $value;

      }
  }  
}

register_globals_on();

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function FormatarValorExibir($valor){
	if(isset($valor)){
	    return number_format($valor,2,",",".");
	}
	else{
		return "0,00";
	}
}

function FormatarValorBanco($valor){  
	return str_replace(",",".",str_replace(".","",$valor));
}

function FormatarDataExibir($data){
    if(!$data) return;
    return substr($data, 6, 2)."/".substr($data, 4, 2)."/".substr($data, 0, 4);
}

function FormatarDataHoraExibir($data){
    if(!$data) return;
    return substr($data, 6, 2)."/".substr($data, 4, 2)."/".substr($data, 0, 4)." ".substr($data, 8, 2).":".substr($data, 10, 2);
}

function FormatarDataBanco($data){
    if(!$data) return "";
    $data = str_replace("/","",$data);
    return substr($data, 4, 4) . substr($data, 2, 2) . substr($data, 0, 2);
}

function FormatarDataHoraBanco($data){
    if(!$data) return "";
    $data = str_replace(":","",$data);
    $data = str_replace("/","",$data);
    $data = str_replace(" ","",$data);
	//123029012014

    return substr($data, 8, 4).substr($data, 6, 2).substr($data, 4, 2).substr($data, 0, 4);
}

?>