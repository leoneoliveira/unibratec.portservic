var usuario = {
	construct:$(function(){
		usuario.cadastrar();
		usuario.login();
	}),
	cadastrar:function(){
		$("#cadastro_usuario").click(function(){
			if($("#cpf").val() == ""){
				$("#cpf").value = null;
			}
			if($("#cnpj").val() == ""){
				$("#cnpj").value = null;
			}
			var serialize = $("#form_cadastro_usuario").serialize();
			$.ajax({
				url:"http://portservise.esy.es/portservise_webservice/UsuarioModel/insertUsuario",
				data:serialize,
				type:"POST",
				dataType:"JSON",
				success:function(data){
					if(data.success){
						console.log(data.User);
						$.ajax({
							url:"/templates/Back/ajax.handle.page/ajax.create.user.php",
							data:{usuario:data.User},
							type:"POST",
							success:function(data){
								window.location ="/templates/Back/";
							},
							error:function(data){
								console.log(data);
							}
						});
					}
				},
				error:function(data){
					console.log(data);
				}
			});
		});
	},
	login:function(){
		$("#login_usuario").click(function(){
			var serialize = $("#form_login_usuario").serialize();
			$.ajax({
				url:"http://portservise.esy.es/portservise_webservice/UsuarioModel/logar",
				data:serialize,
				type:"POST",
				dataType:"JSON",
				success:function(data){
					if(data.success){
						console.log(data.User);
						$.ajax({
							url:"/templates/Back/ajax.handle.page/ajax.create.user.php",
							data:{usuario:data.User},
							type:"POST",
							success:function(data){
								window.location ="/templates/Back/";
							},
							error:function(data){
								console.log(data);
							}
						});
					}
				},
				error:function(data){
					console.log(data);
				}
			});			
		});
	}
}

usuario.construct;